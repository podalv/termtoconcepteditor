package com.podalv.ttce.importtools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

import com.podalv.maps.string.StringKeyIntMap;
import com.podalv.utils.file.FileUtils;

/** Tools to import Paea's slices
 *
 * @author podalv
 *
 */
public class ImportPaea {

  /** This should be changed to reflect the mapid of the first available map */
  public static final int FIRST_MAP_ID = 15;
  /** This should be changed to reflect who will be the owner of created maps */
  public static final int OWNER_ID     = 3;

  private static String[] cleanStr(final String[] data) {
    for (int x = 0; x < data.length; x++) {
      if (data[x].startsWith("\"") && data[x].endsWith("\"")) {
        data[x] = data[x].substring(1, data[x].length() - 1);
      }
    }
    return data;
  }

  public static void main(final String[] args) throws IOException {
    final ArrayList<String> lines = FileUtils.readFileArrayList(new File("/home/podalv/slices.csv"), Charset.forName("UTF-8"));
    final HashMap<Integer, String[]> cuiToMapId = new HashMap<Integer, String[]>();
    int mapId = FIRST_MAP_ID;
    final HashSet<String> addedMaps = new HashSet<String>();
    final StringKeyIntMap mapNameToCnt = new StringKeyIntMap();
    for (int x = 0; x < lines.size(); x++) {
      final String[] data = cleanStr(lines.get(x).split("\t"));
      if (!addedMaps.contains(data[1])) {
        Integer val = mapNameToCnt.get(data[0]);
        if (val == null) {
          val = Integer.valueOf(0);
        }
        mapNameToCnt.put(data[0], val + 1);
        addedMaps.add(data[1]);
        cuiToMapId.put(mapId++, new String[] {data[0], data[1]});
      }
    }

    final Iterator<Entry<Integer, String[]>> iterator = cuiToMapId.entrySet().iterator();
    mapId = FIRST_MAP_ID;
    addedMaps.clear();
    final BufferedWriter output = new BufferedWriter(new FileWriter("/home/podalv/paea.txt"));
    while (iterator.hasNext()) {
      final Entry<Integer, String[]> entry = iterator.next();
      if (mapNameToCnt.get(entry.getValue()[0]) == 1) {
        final int currentMapId = mapId;
        output.write("INSERT INTO term_concept_map.term_to_concept_maps VALUES(" + (mapId++) + ", \"" + entry.getValue()[0] + "\", " + OWNER_ID + ", \"N\", 1, \""
            + entry.getValue()[1] + "\");\n");
        for (int x = 0; x < lines.size(); x++) {
          final String[] data = cleanStr(lines.get(x).split("\t"));
          if (data[0].equals(entry.getValue()[0])) {
            output.write("INSERT INTO term_concept_map.term_to_concept_values VALUES(" + currentMapId + ", \"" + data[2] + "\", \"" + data[3] + "\");\n");
          }
        }
      }
      else {
        if (!addedMaps.contains(entry.getValue()[0])) {
          addedMaps.add(entry.getValue()[0]);
          final int currentMapId = mapId;
          output.write("INSERT INTO term_concept_map.term_to_concept_maps VALUES(" + (mapId++) + ", \"" + entry.getValue()[0] + "\", " + OWNER_ID + ", \"N\", 1, \""
              + entry.getValue()[0] + "\");\n");
          for (int x = 0; x < lines.size(); x++) {
            final String[] data = cleanStr(lines.get(x).split("\t"));
            if (data[0].equals(entry.getValue()[0])) {
              output.write("INSERT INTO term_concept_map.term_to_concept_values VALUES(" + currentMapId + ", \"" + data[2] + "\", \"" + data[3] + "\");\n");
            }
          }
        }
      }
    }
    output.close();
  }
}
