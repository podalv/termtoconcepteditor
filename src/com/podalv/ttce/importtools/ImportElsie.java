package com.podalv.ttce.importtools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.ArrayList;

import com.podalv.db.Database;
import com.podalv.db.datastructures.ConnectionSettings;
import com.podalv.db.datastructures.LoginCredentials;
import com.podalv.maps.string.StringHashSet;
import com.podalv.maps.string.StringKeyObjectMap;
import com.podalv.ttce.tables.Terminology;
import com.podalv.utils.file.FileUtils;

/** Import tool for Elsie's slices (based on Terminology 3)
 *
 * @author podalv
 *
 */
public class ImportElsie {

  public static final int FIRST_MAP_ID = 190;
  public static final int OWNER_ID     = 7;

  private static void cuiMap() throws SQLException, IOException {
    Database.create(new ConnectionSettings("jdbc:mysql://localhost:3306", new LoginCredentials("root", "root")));
    final ArrayList<String> lines = FileUtils.readFileArrayList(new File("/home/podalv/elsie_cui_map.txt"), Charset.forName("UTF-8"));
    final StringKeyObjectMap<StringHashSet> mapNameToCuis = new StringKeyObjectMap<StringHashSet>();
    for (int x = 0; x < lines.size(); x++) {
      final String[] data = lines.get(x).split("\t");
      StringHashSet set = mapNameToCuis.get(data[0]);
      if (set == null) {
        set = new StringHashSet();
        mapNameToCuis.put(data[0], set);
      }
      set.add(data[1]);
    }

    final BufferedWriter output = new BufferedWriter(new FileWriter("/home/podalv/output.sql"));
    final StringHashSet processedMaps = new StringHashSet();
    int currentMapId = FIRST_MAP_ID - 1;
    for (int x = 0; x < lines.size(); x++) {
      final String[] data = lines.get(x).split("\t");
      if (!processedMaps.contains(data[0])) {
        processedMaps.add(data[0]);
        if (mapNameToCuis.get(data[0]).size() == 1) {
          output.write("INSERT INTO term_concept_map.term_to_concept_maps VALUES(" + (++currentMapId) + ", \"" + data[0] + "\", " + OWNER_ID + ", \"N\", 1, \"" + data[1]
              + "\");\n");
        }
        else {
          output.write("INSERT INTO term_concept_map.term_to_concept_maps VALUES(" + (++currentMapId) + ", \"" + data[0] + "\", " + OWNER_ID + ", \"N\", 1, \"" + data[0]
              + "\");\n");
        }
      }
      output.write("INSERT INTO term_concept_map.term_to_concept_values VALUES(" + currentMapId + ", \"" + data[1] + "\", \"" + Terminology.getStringFromTidTerm3(data[2])
          + "\");\n");
    }
    output.close();
  }

  private static void cidMap() throws SQLException, IOException {
    Database.create(new ConnectionSettings("jdbc:mysql://localhost:3306", new LoginCredentials("root", "root")));
    final ArrayList<String> lines = FileUtils.readFileArrayList(new File("/home/podalv/elsie_cid_map.txt"), Charset.forName("UTF-8"));
    final StringKeyObjectMap<StringHashSet> mapNameToCuis = new StringKeyObjectMap<StringHashSet>();
    for (int x = 0; x < lines.size(); x++) {
      final String[] data = lines.get(x).split("\t");
      StringHashSet set = mapNameToCuis.get(data[0]);
      if (set == null) {
        set = new StringHashSet();
        mapNameToCuis.put(data[0], set);
      }
      set.add(data[1]);
    }

    final BufferedWriter output = new BufferedWriter(new FileWriter("/home/podalv/output.sql"));
    final StringHashSet processedMaps = new StringHashSet();
    int currentMapId = FIRST_MAP_ID - 1;
    for (int x = 0; x < lines.size(); x++) {
      final String[] data = lines.get(x).split("\t");
      if (!processedMaps.contains(data[0])) {
        processedMaps.add(data[0]);
        if (mapNameToCuis.get(data[0]).size() == 1) {
          output.write("INSERT INTO term_concept_map.term_to_concept_maps VALUES(" + (++currentMapId) + ", \"" + data[0] + "\", " + OWNER_ID + ", \"N\", 1, \""
              + Terminology.getCuiFromCidTerm3(data[2]) + "\");\n");
        }
        else {
          output.write("INSERT INTO term_concept_map.term_to_concept_maps VALUES(" + (++currentMapId) + ", \"" + data[0] + "\", " + OWNER_ID + ", \"N\", 1, \"" + data[0]
              + "\");\n");
        }
      }
      output.write("INSERT INTO term_concept_map.term_to_concept_values VALUES(" + currentMapId + ", \"" + Terminology.getCuiFromCidTerm3(data[2]) + "\", \""
          + Terminology.getStringFromTidTerm3(data[1]) + "\");\n");
    }
    output.close();
  }

  public static void main(final String[] args) throws IOException, SQLException {
    cuiMap();
    cidMap();
  }

}
