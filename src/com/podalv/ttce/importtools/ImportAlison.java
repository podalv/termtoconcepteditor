package com.podalv.ttce.importtools;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

import com.podalv.db.Database;
import com.podalv.db.datastructures.ConnectionSettings;
import com.podalv.db.datastructures.LoginCredentials;

/** Import of all of Alison's slices from tables ahrq_conditions_tids_expanded and asd_tid_cid_suppress_generous
 *
 * @author podalv
 *
 */
public class ImportAlison {

  public static final int OWNER_ID     = 3;
  public static int       FIRST_MAP_ID = 193;
  static BufferedWriter   output;

  private static void expandedMap() throws SQLException, IOException {
    final ResultSet set = Database.getInstance().query("SELECT * FROM user_acallaha.ahrq_conditions_tids_expanded order by 'condition';");
    String prevCondition = null;
    int currentMapId = FIRST_MAP_ID;
    while (set.next()) {
      if (prevCondition == null) {
        prevCondition = set.getString(1);
      }
      if (!prevCondition.equals(set.getString(1))) {
        currentMapId++;
        prevCondition = set.getString(1);
        output.write("INSERT INTO term_concept_map.term_to_concept_maps VALUES(" + (currentMapId) + ", \"" + prevCondition + "\", " + OWNER_ID + ", \"N\", 1, \"" + prevCondition
            + "\");\n");
      }
      final ResultSet s = Database.getInstance().query(
          "SELECT * FROM terminology3.tid2cid inner join (terminology3.str2cid, terminology3.str2tid) on (terminology3.tid2cid.cid = terminology3.str2cid.cid AND terminology3.tid2cid.tid = terminology3.str2tid.tid) where terminology3.tid2cid.tid="
              + set.getString(2));
      s.next();
      final String text = s.getString(11);
      final String cui = s.getString(8);
      s.close();
      output.write("INSERT INTO term_concept_map.term_to_concept_values VALUES(" + currentMapId + ", \"" + cui + "\", \"" + Database.escapeSql(text) + "\");\n");
    }
    FIRST_MAP_ID = currentMapId + 1;
    set.close();
  }

  private static void suppressMap() throws SQLException, IOException {
    final ResultSet set = Database.getInstance().query("SELECT * FROM user_acallaha.asd_tid_cid_suppress_generous;");
    final int currentMapId = FIRST_MAP_ID;
    final HashMap<String, HashSet<String>> cuiToText = new HashMap<String, HashSet<String>>();
    int cnt = 0;
    while (set.next()) {
      if (cnt++ % 1000 == 0) {
        System.out.println(cnt);
      }
      final ResultSet s = Database.getInstance().query(
          "SELECT * FROM terminology3.str2tid JOIN (terminology3.str2cid, terminology3.tid2cid) ON (terminology3.str2cid.cid = terminology3.tid2cid.cid AND terminology3.tid2cid.tid = terminology3.str2tid.tid) where terminology3.str2tid.tid = "
              + set.getString(1) + " and terminology3.str2cid.cid = " + set.getString(2));
      /*      if (!s.next()) {
              output.write(set.getString(1) + " / " + set.getString(2) + "\n");
              continue;
            }*/
      s.next();
      final String text = s.getString(7);
      final String cui = s.getString(8);
      HashSet<String> ss = cuiToText.get(cui);
      if (ss == null) {
        ss = new HashSet<String>();
        cuiToText.put(cui, ss);
      }
      ss.add(text);
      s.close();
    }
    set.close();

    output.write("INSERT INTO term_concept_map.term_to_concept_maps VALUES(" + (currentMapId) + ", \"ASD\", " + OWNER_ID + ", \"N\", 1, \"ASD\");\n");
    final Iterator<Entry<String, HashSet<String>>> iterator = cuiToText.entrySet().iterator();
    while (iterator.hasNext()) {
      final Entry<String, HashSet<String>> entry = iterator.next();
      final ResultSet cuiStrings = Database.getInstance().query("SELECT * FROM terminology4.cui2str WHERE cui=\"" + entry.getKey() + "\"");
      while (cuiStrings.next()) {
        if (!entry.getValue().contains(cuiStrings.getString(2))) {
          output.write("INSERT INTO term_concept_map.term_to_concept_values VALUES(" + currentMapId + ", \"" + entry.getKey() + "\", \""
              + Database.escapeSql(cuiStrings.getString(2)) + "\");\n");
        }
      }
      cuiStrings.close();

    }
    FIRST_MAP_ID = currentMapId + 1;
  }

  public static void main(final String[] args) throws SQLException, IOException {
    Database.create(new ConnectionSettings("jdbc:mysql://localhost:3306", new LoginCredentials("root", "root")));
    output = new BufferedWriter(new FileWriter("/home/podalv/output.sql"));
    expandedMap();
    suppressMap();
    output.close();
  }

}
