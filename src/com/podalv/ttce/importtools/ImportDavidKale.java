package com.podalv.ttce.importtools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import com.podalv.db.Database;

public class ImportDavidKale {

  public static void main(final String[] args) throws IOException {
    final File file = new File(new File("/home/podalv/"), "david_kale.txt");
    System.out.println(file.getAbsolutePath());
    final BufferedReader reader = new BufferedReader(new FileReader(file));
    String line;
    String prevPhenotype = null;
    int currentMapId = 256;
    while ((line = reader.readLine()) != null) {
      final String[] data = line.split("\t");
      if (prevPhenotype == null || !prevPhenotype.equals(data[4])) {
        prevPhenotype = data[4];
        System.out.println("INSERT INTO term_concept_map.term_to_concept_maps VALUES(" + (++currentMapId) + ", \"" + data[4] + "\", 13, \"N\", 1, \"" + data[5] + "\");");
      }
      System.out.println("INSERT INTO term_concept_map.term_to_concept_values VALUES(" + currentMapId + ", \"" + data[3] + "\", \"" + Database.escapeSql(data[1]) + "\");");

    }
    reader.close();
  }
}
