package com.podalv.ttce.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.session.HashSessionIdManager;
import org.eclipse.jetty.server.session.HashSessionManager;
import org.eclipse.jetty.server.session.SessionHandler;

import com.podalv.db.Database;
import com.podalv.db.datastructures.ConnectionSettings;
import com.podalv.ttce.common.HtmlCommon;
import com.podalv.ttce.common.HtmlCommon.TEMPLATE;
import com.podalv.ttce.common.StringStatsDictionary;
import com.podalv.ttce.tables.Medline;
import com.podalv.ttce.tables.Owners;
import com.podalv.ttce.tables.Stride5;
import com.podalv.ttce.tables.TermToConceptMaps;
import com.podalv.ttce.tables.TermToConceptValues;

public class EditorServer extends AbstractHandler {

  public static final String DB_URL                 = "db_url";
  public static final String TEMPLATES              = "templates";
  public static final String DB_LOGIN               = "db_login";
  public static final String DB_PASSWORD            = "db_password";
  public static final String COMMAND_ADD_USER       = "add_user";
  public static final String COMMAND_MEDLINE        = "medline";
  public static final String COMMAND_STRIDE5        = "stride5";
  public static final String PATH_DEFAULT           = "/";
  public static final String PATH_ADD_NEW_MAP       = "/addNewMap";
  public static final String PATH_LOGOUT            = "/logout";
  public static final String PATH_EDIT_MAP          = "/editMap";
  public static final String PATH_DELETE_MAP        = "/deleteMap";
  public static final String PATH_EDIT_MAP_CONTENTS = "/editMapContents";
  public static final String PATH_CHANGE_PASSWORD   = "/changePassword";
  public static final String PATH_ADD_MAPPING       = "/addMapping";
  public static final String PATH_SAVE_MAPPING      = "/saveMapping";
  public static final String PATH_EDIT_MAPPING      = "/editMapping";
  public static final String PATH_EDIT_ADMIN_STATUS = "/editAdminStatus";
  public static final String PATH_RESET_PASSWORD    = "/resetPassword";
  public static final String PATH_ADMIN             = "/admin";
  public static final String PATH_ADD_USER          = "/addUser";
  public static final String PATH_COPY              = "/copyMap";

  @Override
  public void handle(final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException {
    response.setContentType("text/html;charset=utf-8");
    response.setStatus(HttpServletResponse.SC_OK);
    baseRequest.setHandled(true);
    if (Database.getInstance() == null) {
      response.sendError(404, "Could not connect to the database. Please contact your administrator");
      return;
    }
    final boolean loginCorrect = Login.checkLogin(request);
    if (!loginCorrect) {
      response.getWriter().println(HtmlCommon.generatePage(TEMPLATE.LOGIN, request));
    }
    else if (target.equals(PATH_CHANGE_PASSWORD)) {
      if (!Login.changePassword(request)) {
        response.getWriter().println(HtmlCommon.generatePage(TEMPLATE.CHANGE_PASSWORD, request));
      }
      else {
        response.sendRedirect(PATH_DEFAULT);
      }
    }
    else if (Login.isDefaultPasswordLogin(request)) {
      response.getWriter().println(HtmlCommon.generatePage(TEMPLATE.CHANGE_PASSWORD, request));
    }
    else if (target.equals(PATH_DEFAULT)) {
      Maps.displayMaps(request, response);
    }
    else if (target.equals(PATH_ADD_NEW_MAP)) {
      EditMap.editMap(request, response);
    }
    else if (target.equals(PATH_EDIT_MAP)) {
      EditMap.saveMapChanges(request, response);
      Maps.displayMaps(request, response);
    }
    else if (target.equals(PATH_DELETE_MAP)) {
      EditMap.deleteMap(request, response);
      return;
    }
    else if (target.equals(PATH_EDIT_MAP_CONTENTS)) {
      EditMapValues.displayMappings(request, response);
    }
    else if (target.equals(PATH_ADD_MAPPING)) {
      EditMapValues.displayAddMapping(request, response);
    }
    else if (target.equals(PATH_EDIT_MAPPING)) {
      EditMapValues.displayEditMapping(request, response);
    }
    else if (target.equals(PATH_SAVE_MAPPING)) {
      EditMapValues.saveMapping(request, response);
    }
    else if (target.equals(PATH_ADMIN)) {
      Admin.displayAdminPage(request, response);
    }
    else if (target.equals(PATH_RESET_PASSWORD)) {
      Admin.resetPassword(request, response);
    }
    else if (target.equals(PATH_EDIT_ADMIN_STATUS)) {
      Admin.editAdminStatus(request, response);
    }
    else if (target.equals(PATH_LOGOUT)) {
      Login.logout(request);
      response.sendRedirect("/");
    }
    else if (target.equals(PATH_ADD_USER)) {
      Admin.addUser(request, response);
    }
    else if (target.equals(PATH_COPY)) {
      EditMap.copyMap(request, response);
    }
  }

  private static void initDatabase() throws FileNotFoundException, IOException, SQLException {
    try {
      final Properties props = new Properties();
      props.loadFromXML(new FileInputStream("settings"));
      Database.create(new ConnectionSettings(props.getProperty(DB_URL), new com.podalv.db.datastructures.LoginCredentials(props.getProperty(DB_LOGIN), props.getProperty(DB_PASSWORD))));
      Owners.create();
      TermToConceptMaps.create();
      TermToConceptValues.create();
    }
    catch (final Exception e) {
      e.printStackTrace();
      System.err.println("Could not read the 'settings' file. It should be located in the '" + new File(".").getAbsolutePath() + "' with name 'settings'");
      System.err.println("Creating new one now...");
      final Properties props = new Properties();
      props.put(DB_URL, "jdbc:mysql://localhost:3306");
      props.put(DB_LOGIN, "login");
      props.put(DB_PASSWORD, "password");
      props.storeToXML(new FileOutputStream("settings"), "");
    }
  }

  private static void runCommands(final String[] args) throws SQLException {
    if (args.length != 0) {
      if (args[0].equals(COMMAND_ADD_USER)) {
        if (args.length != 2) {
          System.out.println("Error executing command. Not enough parameters");
        }
        else if (!Owners.createOwner(args[1], Owners.DEFAULT_PASSWORD)) {
          System.out.println("Error creating user with this name. The name is already taken");

        }
      }
      if (args[0].equals(COMMAND_MEDLINE)) {
        Medline.generateMedlineStats();
      }
      if (args[0].equals(COMMAND_STRIDE5)) {
        Stride5.generatePatientFrequencies();
      }
      System.exit(0);
    }
  }

  public static void main(final String[] args) throws Exception {
    System.out.println("Usage: COMMAND PARAM1 PARAM2 ...");
    System.out.println();
    System.out.println("COMMANDS: add_user username");
    System.out.println("          medline");
    System.out.println("          stride5");
    System.out.println();
    System.out.println("add_user - adds a new user with the specified name and the 'password' password");
    System.out.println("medline  - recalculates the medline frequencies and stores the results into a table");
    System.out.println("stride5  - recalculates the stride5 frequencies to generate non-negated patient frequencies");

    initDatabase();
    runCommands(args);
    StringStatsDictionary.readData();

    final Server server = new Server(8080);
    final EditorServer editor = new EditorServer();
    server.setHandler(new EditorServer());

    final HashSessionIdManager idmanager = new HashSessionIdManager();
    server.setSessionIdManager(idmanager);

    final ContextHandler context = new ContextHandler("/");
    server.setHandler(context);

    final HashSessionManager manager = new HashSessionManager();
    final SessionHandler sessions = new SessionHandler(manager);
    context.setHandler(sessions);

    sessions.setHandler(editor);

    server.start();
    server.join();
  }
}
