package com.podalv.ttce.server;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.podalv.db.utils.HashUtils;
import com.podalv.ttce.tables.Owners;

/** Handles sessions and logging
 *
 * @author podalv
 *
 */
public class Login {

  public static final String LOGGED_USERNAME             = "loggedUserName";
  public static final String LOGGED_USERID               = "loggedUserId";
  public static final String LOGGED_ADMIN                = "loggedAdmin";
  public static final String LOGGED_VALID_PASSWORD       = "validPassword";
  public static final String INVALID_NAME                = "errorLogin";
  public static final String INVALID_PASSWORDS_NOT_EQUAL = "errorChangePasswordNotEqual";
  public static final String INVALID_ORIGINAL_PASSWORD   = "errorChangePasswordInvalidOriginal";
  public static final String INVALID_NEW_PASSWORD        = "errorChangePasswordInvalidNewPassword";

  public static boolean checkLogin(final HttpServletRequest request) {
    final HttpSession session = request.getSession();
    if (session.getAttribute(LOGGED_USERID) == null || session.getAttribute(LOGGED_USERNAME) == null) {
      final String name = request.getParameter("username");
      final String password = request.getParameter("password");
      if (name != null && password != null && Owners.checkLogin(name, password)) {
        try {
          final int userId = Owners.getOwnerId(request.getParameter("username"));
          session.setAttribute(LOGGED_USERID, userId);
          session.setAttribute(LOGGED_USERNAME, name);
          request.removeAttribute("username");
          request.removeAttribute("password");
          session.removeAttribute(INVALID_NAME);
          session.removeAttribute(LOGGED_ADMIN);
          if (Owners.getOwnerGroup(userId).equals(Owners.ADMIN_GROUP)) {
            session.setAttribute(LOGGED_ADMIN, true);
          }
          return true;
        }
        catch (final SQLException e) {
          e.printStackTrace();
        }
        session.setAttribute(INVALID_NAME, name);
      }
      session.setAttribute(INVALID_NAME, name);
    }
    else {
      return true;
    }
    return false;
  }

  public static boolean changePassword(final HttpServletRequest request) {
    final int ownerId = (int) request.getSession().getAttribute(LOGGED_USERID);
    final String originalPassword = request.getParameter("previousPassword");
    final String newPassword1 = request.getParameter("newPassword1");
    final String newPassword2 = request.getParameter("newPassword2");

    try {
      final String passwordHash = Owners.selectPasswordFromOwners(ownerId);
      if (!passwordHash.equals(HashUtils.calculateHash(originalPassword))) {
        request.getSession().setAttribute(INVALID_ORIGINAL_PASSWORD, "true");
        return false;
      }
    }
    catch (final Exception e) {
      return false;
    }
    request.getSession().removeAttribute(INVALID_ORIGINAL_PASSWORD);
    if (!newPassword1.equals(newPassword2)) {
      request.getSession().setAttribute(INVALID_PASSWORDS_NOT_EQUAL, "true");
      return false;
    }
    request.getSession().removeAttribute(INVALID_PASSWORDS_NOT_EQUAL);
    if (newPassword1.equals(originalPassword)) {
      request.getSession().setAttribute(INVALID_NEW_PASSWORD, "true");
      return false;
    }
    request.getSession().removeAttribute(INVALID_NEW_PASSWORD);
    Owners.setPassword(ownerId, newPassword1);
    return true;
  }

  public static boolean checkPasswordChange(final HttpServletRequest request) {
    final HttpSession session = request.getSession();
    if (session.getAttribute(LOGGED_USERID) != null || session.getAttribute(LOGGED_USERNAME) != null) {
      final String name = request.getParameter("username");
      final String password = request.getParameter("password");
      if (name != null && password != null && Owners.checkLogin(name, password)) {
        try {
          final int userId = Owners.getOwnerId(request.getParameter("username"));
          session.setAttribute(LOGGED_USERID, userId);
          session.setAttribute(LOGGED_USERNAME, name);
          request.removeAttribute("username");
          request.removeAttribute("password");
          session.removeAttribute(INVALID_NAME);
          return true;
        }
        catch (final SQLException e) {
          e.printStackTrace();
        }
        session.setAttribute(INVALID_NAME, name);
      }
      session.setAttribute(INVALID_NAME, name);
    }
    else {
      return true;
    }
    return false;
  }

  public static void logout(final HttpServletRequest request) {
    request.getSession().removeAttribute(LOGGED_ADMIN);
    request.getSession().removeAttribute(LOGGED_USERID);
    request.getSession().removeAttribute(LOGGED_USERNAME);
  }

  public static boolean isDefaultPasswordLogin(final HttpServletRequest request) {
    try {
      final HttpSession session = request.getSession();
      if (session.getAttribute(LOGGED_VALID_PASSWORD) == null && (session.getAttribute(LOGGED_USERID) != null || session.getAttribute(LOGGED_USERNAME) != null)) {
        final Integer ownerId = (Integer) session.getAttribute(LOGGED_USERID);
        final boolean isDefaultPassword = Owners.isPasswordEqual(Owners.selectPasswordFromOwners(ownerId), HashUtils.calculateHash(Owners.DEFAULT_PASSWORD));
        if (isDefaultPassword) {
          session.removeAttribute(LOGGED_VALID_PASSWORD);
        }
        else {
          session.setAttribute(LOGGED_VALID_PASSWORD, !isDefaultPassword);
        }
        return isDefaultPassword;
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    return false;
  }
}
