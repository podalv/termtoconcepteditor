package com.podalv.ttce.server;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.podalv.ttce.common.HtmlCommon;
import com.podalv.ttce.common.HtmlCommon.TEMPLATE;
import com.podalv.ttce.common.HtmlTable;
import com.podalv.ttce.common.StringStats;
import com.podalv.ttce.common.StringStatsDictionary;
import com.podalv.ttce.common.Tables;
import com.podalv.ttce.tables.Owners;
import com.podalv.ttce.tables.TermToConceptMaps;
import com.podalv.ttce.tables.TermToConceptValues;
import com.podalv.ttce.tables.Terminology;

/** All the code that allows editing / creating
 *
 * @author podalv
 *
 */
public class EditMapValues {

  public static final String CUI_ALREADY_DEFINED_WARNING = "cuiDefinedWarning";

  public static void saveMapping(final HttpServletRequest request, final HttpServletResponse response) {
    try {
      final int mapId = Integer.parseInt(request.getParameter(TermToConceptMaps.TTCM_MAPID).toString());
      final String cui = request.getParameter(TermToConceptMaps.CUI2STR_CUI).toString();
      final HashSet<Integer> checked = new HashSet<Integer>();
      for (int x = 0; x < 1000; x++) {
        if (request.getParameter(x + "") != null) {
          checked.add(x);
        }
      }
      TermToConceptValues.deleteCui(mapId, cui);
      final ResultSet set = Terminology.selectMultipleCuis(cui);
      int cnt = 0;
      if (set != null) {
        while (set.next()) {
          if (checked.contains(cnt)) {
            TermToConceptValues.createValueSql(mapId, cui, set.getString(TermToConceptValues.CUI2STR_STR));
          }
          cnt++;
        }
        set.close();
      }
      response.sendRedirect(EditorServer.PATH_EDIT_MAP_CONTENTS + "?" + TermToConceptMaps.TTCM_MAPID + "=" + mapId);
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
  }

  private static HashMap<StringStats, String[]> getSortedRows(final ResultSet set, final int mapId, final String cui) throws SQLException {
    final HashMap<StringStats, String[]> result = new HashMap<StringStats, String[]>();
    final HashSet<String> existing = TermToConceptValues.selectStrings(mapId, cui);
    int cnt = 0;
    if (set != null) {
      while (set.next()) {
        final String text = set.getString(TermToConceptMaps.CUI2STR_STR);
        final String checked = existing.contains(text) ? "checked" : "";
        final StringStats stats = StringStatsDictionary.getStats(text.toLowerCase());
        result.put(stats, new String[] {text, cnt + "", checked, stats.getStride5Single(), stats.getStride5All(), stats.getMedline(), stats.getSyntacticType(),
            stats.getUniqueCuiCnt(), stats.getSemanticTypeCnt(), stats.getSemanticGroupCnt()});
        cnt++;
      }
      set.close();
    }
    return result;
  }

  private static void addRowsToTable(final HtmlTable currentTable, final HashMap<StringStats, String[]> sortedRows, final boolean empty) {
    final Iterator<Entry<StringStats, String[]>> iterator = sortedRows.entrySet().iterator();
    while (iterator.hasNext()) {
      final Entry<StringStats, String[]> entry = iterator.next();
      if (entry.getKey().isEmpty() == empty) {
        currentTable.addRow(entry.getValue());
      }
    }
  }

  public static void displayEditMapping(final HttpServletRequest request, final HttpServletResponse response) {
    try {
      final int mapId = Integer.parseInt(request.getParameter(TermToConceptMaps.TTCM_MAPID).toString());
      final String cui = request.getParameter(TermToConceptMaps.STR2CUI_CUI).toString();
      final HtmlTable currentTable = new HtmlTable("CUI", "Term", "S5 term freq(%)", "S5 patient freq(%)", "Medline freq(%)", "NP (%)", "CUI cnt", "STY cnt", "SG cnt", "Keep");
      final ResultSet set = Terminology.selectMultipleCuis(cui);

      request.getSession().removeAttribute(CUI_ALREADY_DEFINED_WARNING);
      final ResultSet definedCuis = TermToConceptValues.selectCui(cui);
      final HashSet<Integer> definedInMultipleMaps = new HashSet<Integer>();
      if (definedCuis.next()) {
        if (definedCuis.getInt(Tables.TTCV_MAPID) != mapId) {
          definedInMultipleMaps.add(definedCuis.getInt(Tables.TTCV_MAPID));
        }
      }

      if (definedInMultipleMaps.size() != 0) {
        final StringBuilder warning = new StringBuilder("Warning ! " + cui + " is already defined in a map '"
            + TermToConceptMaps.selectSingleMapName(definedCuis.getInt(TermToConceptMaps.TTCV_MAPID)) + "' by "
            + Owners.getOwnerName(TermToConceptMaps.selectSingleOwnerId(definedCuis.getInt(TermToConceptMaps.TTCV_MAPID))));
        if (definedInMultipleMaps.size() > 1) {
          warning.append(" and " + (definedInMultipleMaps.size() - 1) + " others");
        }
        request.getSession().setAttribute(CUI_ALREADY_DEFINED_WARNING, warning.toString());
      }

      final HashMap<StringStats, String[]> sortedRows = getSortedRows(set, mapId, cui);
      addRowsToTable(currentTable, sortedRows, false);
      addRowsToTable(currentTable, sortedRows, true);

      final HashMap<String, Object> values = HtmlCommon.createSingleValueMap("map", currentTable);
      values.put("mapName", TermToConceptMaps.selectSingleMapName(mapId));
      response.getWriter().println(HtmlCommon.generatePage(TEMPLATE.EDIT_MAPPING, request, values));
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
  }

  private static HashSet<String> getExistingCuis(final int mapId) throws SQLException {
    return new HashSet<String>(TermToConceptValues.selectCuis(mapId));
  }

  public static void displayAddMapping(final HttpServletRequest request, final HttpServletResponse response) {
    try {
      final int mapId = Integer.parseInt(request.getParameter(TermToConceptMaps.TTCM_MAPID).toString());
      String searchString = request.getParameter("searchString");
      if (searchString == null) {
        searchString = "";
      }
      final HashMap<String, Object> values = HtmlCommon.createSingleValueMap("mapName", TermToConceptMaps.selectSingleMapName(mapId));
      final HashSet<String> existingCuis = getExistingCuis(mapId);

      final HtmlTable exactTable = new HtmlTable("CUI", "Term", "Ontology", "Suppress", "Candidate terms", "Action");
      final HtmlTable approximateTable = new HtmlTable("CUI", "Term", "Ontology", "Suppress", "Candidate terms", "Action");
      values.put("exactMatchTable", exactTable);
      values.put("approximateMatchTable", approximateTable);
      if (!searchString.isEmpty()) {
        ResultSet set = Terminology.selectCuisFromTextEqual(searchString);
        while (set.next()) {
          final String cui = set.getString(TermToConceptMaps.STR2CUI_CUI);
          final String text = set.getString(TermToConceptMaps.STR2CUI_STR);
          final String ontology = set.getString(TermToConceptMaps.STR2CUI_ONTOLOGY);
          final String suppress = set.getString(TermToConceptMaps.STR2CUI_SUPPRESS);
          final int candidateTermsCnt = StringStatsDictionary.getEligibleTermCnt(cui);
          if (!existingCuis.contains(cui)) {
            exactTable.addRow(cui, text, ontology, suppress, (candidateTermsCnt == 0 ? "" : "" + candidateTermsCnt));
          }
        }
        set.close();
        values.put("exactMatchTable", exactTable);

        set = Terminology.selectCuisFromTextLike(searchString);
        while (set.next()) {
          final String cui = set.getString(TermToConceptMaps.STR2CUI_CUI);
          final String text = set.getString(TermToConceptMaps.STR2CUI_STR);
          final String ontology = set.getString(TermToConceptMaps.STR2CUI_ONTOLOGY);
          final String suppress = set.getString(TermToConceptMaps.STR2CUI_SUPPRESS);
          final int candidateTermsCnt = StringStatsDictionary.getEligibleTermCnt(cui);
          if (!text.equalsIgnoreCase(searchString) && !existingCuis.contains(cui)) {
            approximateTable.addRow(cui, text, ontology, suppress, (candidateTermsCnt == 0 ? "" : "" + candidateTermsCnt));
          }
        }
        set.close();
      }
      response.getWriter().println(HtmlCommon.generatePage(TEMPLATE.SEARCH_MAPPING, request, values));
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
  }

  private static String getNotSelectedMessage(final int cnt) {
    if (cnt < 2) {
      return cnt + " term was not selected...";
    }
    return cnt + " terms were not selected...";
  }

  public static void displayMappings(final HttpServletRequest request, final HttpServletResponse response) {
    try {
      final int mapId = Integer.parseInt(request.getParameter(TermToConceptMaps.TTCM_MAPID).toString());
      final HashMap<String, Object> values = HtmlCommon.createSingleValueMap("mapName", TermToConceptMaps.selectSingleMapName(mapId));
      final LinkedList<HtmlTable> tables = new LinkedList<HtmlTable>();

      HtmlTable currentTable = null;

      ResultSet set = TermToConceptValues.selectSingle(mapId);
      final HashMap<String, HashSet<String>> cuiToSelected = new HashMap<String, HashSet<String>>();
      while (set.next()) {
        final String code = set.getString(TermToConceptValues.TTCV_CODE);
        final String text = set.getString(TermToConceptValues.TTCV_TEXT);
        HashSet<String> s = cuiToSelected.get(code);
        if (s == null) {
          s = new HashSet<String>();
          cuiToSelected.put(code, s);
        }
        s.add(text);
      }

      String prevCode = null;
      set = Terminology.selectMultipleCuis(cuiToSelected.keySet().toArray(new String[cuiToSelected.keySet().size()]));
      int hiddenCnt = 0;
      if (set != null) {
        while (set.next()) {
          final String code = set.getString(TermToConceptValues.CUI2STR_CUI);
          if (prevCode == null) {
            currentTable = new HtmlTable("CUI", "Term", code);
            final HashSet<String> texts = cuiToSelected.get(code);
            final Iterator<String> iterator = texts.iterator();
            while (iterator.hasNext()) {
              currentTable.addRow(code, HtmlCommon.TTCM_VALUES_SELECTED, iterator.next());
            }
            prevCode = code;
          }
          if (!code.equals(prevCode)) {
            if (hiddenCnt != 0) {
              currentTable.addRow("", HtmlCommon.TTCM_VALUES_NORMAL, getNotSelectedMessage(hiddenCnt));
            }
            tables.add(currentTable);
            currentTable = new HtmlTable("CUI", "Term", code);
            final HashSet<String> texts = cuiToSelected.get(code);
            final Iterator<String> iterator = texts.iterator();
            while (iterator.hasNext()) {
              currentTable.addRow(code, HtmlCommon.TTCM_VALUES_SELECTED, iterator.next());
            }
            hiddenCnt = 0;
          }
          prevCode = code;
          final String text = set.getString(TermToConceptMaps.CUI2STR_STR);
          final HashSet<String> texts = cuiToSelected.get(code);
          if (!texts.contains(text)) {
            //currentTable.addRow(code, HtmlCommon.TTCM_VALUES_NORMAL, text);
            hiddenCnt++;
          }
        }
        if (currentTable != null) {
          if (hiddenCnt != 0) {
            currentTable.addRow("", HtmlCommon.TTCM_VALUES_NORMAL, getNotSelectedMessage(hiddenCnt));
          }
          tables.add(currentTable);
        }
        set.close();
      }

      values.put("maps", tables);
      response.getWriter().println(HtmlCommon.generatePage(TEMPLATE.DISPLAY_MAPPINGS, request, values));
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
  }
}
