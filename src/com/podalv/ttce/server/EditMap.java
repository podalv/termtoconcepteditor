package com.podalv.ttce.server;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.podalv.db.Database;
import com.podalv.ttce.common.HtmlCommon;
import com.podalv.ttce.common.HtmlCommon.TEMPLATE;
import com.podalv.ttce.tables.TermToConceptMaps;

/** All the code that edits and creates maps themselves
 *
 * @author podalv
 *
 */
public class EditMap {

  public static void saveMapChanges(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
    int mapId = -1;
    try {
      mapId = Integer.parseInt(request.getParameter(TermToConceptMaps.TTCM_MAPID));
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    try {
      final String mapName = request.getParameter(TermToConceptMaps.TTCM_MAPNAME);
      final String version = request.getParameter(TermToConceptMaps.TTCM_VERSION);
      final String experimental = request.getParameter(TermToConceptMaps.TTCM_EXPERIMENTAL);
      final String queryText = request.getParameter(TermToConceptMaps.TTCM_QUERY_TEXT);

      if (mapId == -1) {
        TermToConceptMaps.createMapSql(mapName, version, experimental, Integer.parseInt(request.getSession().getAttribute(Login.LOGGED_USERID).toString()), queryText);
      }
      else {
        TermToConceptMaps.editMap(mapId, mapName, version, experimental, queryText);
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
    }

  }

  private static boolean deleteMapAction(final HttpServletRequest request, final HttpServletResponse response, final int editingMapId) throws IOException {
    if (request.getParameter("delete") != null && request.getParameter("delete").toString().equals("1")) {
      TermToConceptMaps.deleteMap(editingMapId);
      response.sendRedirect(EditorServer.PATH_DEFAULT);
      return true;
    }
    return false;
  }

  public static void deleteMap(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
    int editingMapId = -1;
    String mapName = "";
    try {
      editingMapId = Integer.parseInt(request.getParameter(TermToConceptMaps.TTCM_MAPID));
      final ResultSet set = TermToConceptMaps.selectSingle(editingMapId);
      set.next();
      mapName = set.getString(TermToConceptMaps.TTCM_MAPNAME);
      Database.closeQuery(set);
    }
    catch (final Exception e) {
      // new map
    }

    if (deleteMapAction(request, response, editingMapId)) {
      return;
    }

    response.getWriter().println(HtmlCommon.generatePage(TEMPLATE.DELETE_MAP, request, HtmlCommon.createSingleValueMap("mapName", mapName)));
  }

  public static void copyMap(final HttpServletRequest request, final HttpServletResponse response) {
    try {
      final int editingMapId = Integer.parseInt(request.getParameter(TermToConceptMaps.TTCM_MAPID));
      TermToConceptMaps.copy(editingMapId, (Integer) request.getSession().getAttribute(Login.LOGGED_USERID));
      response.sendRedirect("/");
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
  }

  public static void editMap(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
    int editingMapId = -1;
    String mapName = "";
    String version = "1";
    String experimental = "Y";
    String queryText = "";
    try {
      editingMapId = Integer.parseInt(request.getParameter(TermToConceptMaps.TTCM_MAPID));
      final ResultSet set = TermToConceptMaps.selectSingle(editingMapId);
      set.next();
      mapName = set.getString(TermToConceptMaps.TTCM_MAPNAME);
      version = set.getString(TermToConceptMaps.TTCM_VERSION);
      experimental = set.getString(TermToConceptMaps.TTCM_EXPERIMENTAL);
      queryText = set.getString(TermToConceptMaps.TTCM_QUERY_TEXT);
      Database.closeQuery(set);
    }
    catch (final Exception e) {
      // new map
    }
    final HashMap<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("mapname", mapName);
    parameters.put("version", version);
    parameters.put("experimental", experimental);
    parameters.put("querytext", queryText);
    response.getWriter().println(HtmlCommon.generatePage(TEMPLATE.EDIT_MAP, request, parameters));
  }
}
