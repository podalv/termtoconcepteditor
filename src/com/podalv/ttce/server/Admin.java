package com.podalv.ttce.server;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.podalv.db.utils.HashUtils;
import com.podalv.ttce.common.HtmlCommon;
import com.podalv.ttce.common.HtmlCommon.TEMPLATE;
import com.podalv.ttce.common.HtmlTable;
import com.podalv.ttce.common.Tables;
import com.podalv.ttce.tables.Owners;

/** Allows making owners admins + resetting their passwords
 *
 * @author podalv
 *
 */
public class Admin extends Tables {

  public static final String OWNER_MESSAGE = "ownerMessage";

  public static void displayAdminPage(final HttpServletRequest request, final HttpServletResponse response) {
    final String message = (String) request.getSession().getAttribute(OWNER_MESSAGE);
    final HashMap<String, Object> values = HtmlCommon.createSingleValueMap(OWNER_MESSAGE, message);
    request.getSession().removeAttribute(OWNER_MESSAGE);
    final HtmlTable table = new HtmlTable("ID", "Name", "Password", "Actions");
    try {
      final ResultSet owners = Owners.selectAll();
      while (owners.next()) {
        String makeAdmin = "Grant admin privileges";
        if (owners.getString(OWNERS_GROUP) != null && owners.getString(OWNERS_GROUP).equals(Owners.ADMIN_GROUP)) {
          makeAdmin = "Revoke admin privileges";
        }
        String password = "Set";
        if (owners.getString(OWNERS_PASSWORD).equals(HashUtils.calculateHash(Owners.DEFAULT_PASSWORD))) {
          password = "Default";
        }
        table.addRow(owners.getString(OWNERS_OWNERID), owners.getString(OWNERS_NAME), password, makeAdmin);
      }
      owners.close();
      values.put("owners", table);
      response.getWriter().println(HtmlCommon.generatePage(TEMPLATE.ADMIN, request, values));
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
  }

  public static void editAdminStatus(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
    if (Owners.isCurrentOwnerAdmin(request)) {
      try {
        final int ownerId = Integer.parseInt(request.getParameter("ownerid").toString());
        final String group = Owners.getOwnerGroup(ownerId);
        if (group == null || !group.equals(Owners.ADMIN_GROUP)) {
          Owners.setGroup(ownerId, Owners.ADMIN_GROUP);
        }
        else {
          Owners.setGroup(ownerId, "");
        }
        request.getSession().setAttribute(OWNER_MESSAGE, "Admin status for owner " + ownerId + " was changed");
      }
      catch (final Exception e) {
        request.getSession().setAttribute(OWNER_MESSAGE, "There was an error while changing admin status");
        e.printStackTrace();
      }
    }
    else {
      request.getSession().setAttribute(OWNER_MESSAGE, "You do not have the right permissions to modify admin status");
    }
    response.sendRedirect(EditorServer.PATH_ADMIN);
  }

  public static void resetPassword(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
    if (Owners.isCurrentOwnerAdmin(request)) {
      try {
        final int ownerId = Integer.parseInt(request.getParameter("ownerid").toString());
        Owners.setPassword(ownerId, Owners.DEFAULT_PASSWORD);
        request.getSession().setAttribute(OWNER_MESSAGE, "Password for owner " + ownerId + " was reset");
      }
      catch (final Exception e) {
        request.getSession().setAttribute(OWNER_MESSAGE, "There was an error while resetting password");
        e.printStackTrace();
      }
    }
    else {
      request.getSession().setAttribute(OWNER_MESSAGE, "You do not have the right permissions to reset password");
    }
    response.sendRedirect(EditorServer.PATH_ADMIN);
  }

  public static void addUser(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
    if (Owners.isCurrentOwnerAdmin(request)) {
      try {
        final String ownerName = request.getParameter("name").toString();
        if (Owners.createOwner(ownerName, Owners.DEFAULT_PASSWORD)) {
          request.getSession().setAttribute(OWNER_MESSAGE, "New owner was created");
        }
        else {
          request.getSession().setAttribute(OWNER_MESSAGE, "Error creating owner, does owner with this name already exists ?");
        }
      }
      catch (final Exception e) {
        request.getSession().setAttribute(OWNER_MESSAGE, "There was an error while creating the new owner");
        e.printStackTrace();
      }
    }
    else {
      request.getSession().setAttribute(OWNER_MESSAGE, "You do not have the right permissions to create an owner");
    }
    response.sendRedirect(EditorServer.PATH_ADMIN);
  }

}
