package com.podalv.ttce.server;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.podalv.db.Database;
import com.podalv.ttce.common.HtmlCommon;
import com.podalv.ttce.common.HtmlCommon.TEMPLATE;
import com.podalv.ttce.common.HtmlTable;
import com.podalv.ttce.tables.TermToConceptMaps;

/** TermToConceptMaps HTML behavior
 *
 * @author podalv
 *
 */
public class Maps {

  public static final int NEW_MAP_ID = -1;

  private static String getRef(final String link, final String mapId, final String text) {
    return "<a href=\"" + link + "?" + TermToConceptMaps.TTCM_MAPID + "=" + mapId + "\" class=\"tableLink\">" + text + "</a>";
  }

  private static HashMap<String, Object> generateMapsTable(final HttpServletRequest request) {
    final HtmlTable result = new HtmlTable("Name", "Version", "Experimental", "Query variable", "Actions");
    ResultSet set = null;
    try {
      set = TermToConceptMaps.selectAllForOwner((int) request.getSession().getAttribute(Login.LOGGED_USERID));
    }
    catch (final Exception e) {
      e.printStackTrace();
    }

    if (set != null) {
      try {
        while (set.next()) {
          result.addRow(new String[] {set.getString(TermToConceptMaps.TTCM_MAPNAME), set.getString(TermToConceptMaps.TTCM_VERSION),
              set.getString(TermToConceptMaps.TTCM_EXPERIMENTAL), set.getString(TermToConceptMaps.TTCM_QUERY_TEXT),
              getRef(EditorServer.PATH_ADD_NEW_MAP, set.getString(TermToConceptMaps.TTCM_MAPID), "EDIT"),
              getRef(EditorServer.PATH_DELETE_MAP, set.getString(TermToConceptMaps.TTCM_MAPID), "DELETE"),
              getRef(EditorServer.PATH_EDIT_MAP_CONTENTS, set.getString(TermToConceptMaps.TTCM_MAPID), "MAPPINGS"),
              getRef(EditorServer.PATH_COPY, set.getString(TermToConceptMaps.TTCM_MAPID), "MAKE A COPY")});
        }
      }
      catch (final Exception e) {
        e.printStackTrace();
      }
    }

    Database.closeQuery(set);
    return HtmlCommon.createSingleValueMap("maps", result);
  }

  public static void displayMaps(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
    response.getWriter().println(HtmlCommon.generatePage(TEMPLATE.MAPS, request, generateMapsTable(request)));
  }

}
