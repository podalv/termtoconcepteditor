package com.podalv.ttce.tables;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.podalv.db.Database;
import com.podalv.ttce.common.Tables;

public class Mayo extends Tables {

  private static final String MAYO_DATABASE = "mayo";
  private static final String STR2CUI_TABLE = "str2cui";
  private static final String STR2CUI_TERM  = "term";
  private static final String STR2CUI_TF    = "tf";

  public static ResultSet selectAll() throws SQLException {
    return Database.getInstance().query(selectWhatFrom(STR2CUI_TERM + ", " + STR2CUI_TF, MAYO_DATABASE + "." + STR2CUI_TABLE));
  }
}
