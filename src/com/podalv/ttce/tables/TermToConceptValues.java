package com.podalv.ttce.tables;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;

import com.podalv.db.Database;
import com.podalv.ttce.common.Tables;

/** Manages the term_to_concept_values table of the ttc mappings
*
* @author podalv
*
*/
public class TermToConceptValues extends Tables {

  private static final String createTermToConceptValuesTable() {
    return "CREATE TABLE IF NOT EXISTS " + DATABASE_NAME + "." + TTCV_TABLE + " (" + //
        "`" + TTCV_MAPID + "` int(11) NOT NULL," + //
        "`" + TTCV_CODE + "` varchar(255) DEFAULT NULL," + //
        "`" + TTCV_TEXT + "` TEXT DEFAULT NULL" + //
        ") ENGINE=MyISAM DEFAULT CHARSET=utf8;";
  }

  public static void copyMaps(final int srcMapId, final int tgtMapId) throws SQLException {
    final ResultSet set = selectSingle(srcMapId);
    while (set.next()) {
      createValueSql(tgtMapId, set.getString(TTCV_CODE), set.getString(TTCV_TEXT));
    }
    set.close();
  }

  public static ResultSet selectCui(final String code) throws SQLException {
    return Database.getInstance().query(Tables.selectWhatFrom("*", TTCV_TABLE) + Tables.whereStringEquals(TTCV_CODE, code));
  }

  public static void createValueSql(final int mapId, final String code, final String text) {
    Database.getInstance().update("INSERT INTO " + DATABASE_NAME + "." + TTCV_TABLE + " VALUES(" + mapId + ", \"" + code + "\", \"" + text + "\");");
  }

  public static void deleteCui(final int mapId, final String code) {
    Database.getInstance().update("DELETE FROM " + DATABASE_NAME + "." + TTCV_TABLE + " WHERE " + TTCV_MAPID + "=" + mapId + " AND " + TTCV_CODE + "=\"" + code + "\"");
  }

  public static void deleteSingle(final int mapId) {
    Database.getInstance().update(Tables.deleteFromWhere(TTCV_TABLE) + Tables.whereIntEquals(TTCV_MAPID, mapId));
  }

  public static ResultSet selectAll() throws SQLException {
    return Database.getInstance().query(Tables.selectWhatFrom("*", TTCV_TABLE));
  }

  public static ResultSet selectSingle(final int mapId) throws SQLException {
    return Database.getInstance().query(Tables.selectWhatFrom("*", TTCV_TABLE) + Tables.whereIntEquals(TTCV_MAPID, mapId) + " ORDER BY " + TTCV_CODE);
  }

  public static HashSet<String> selectStrings(final int mapId, final String code) throws SQLException {
    final HashSet<String> result = new HashSet<String>();
    final ResultSet set = Database.getInstance().query(
        Tables.selectWhatFrom(TTCV_TEXT, TTCV_TABLE) + Tables.whereIntEquals(TTCV_MAPID, mapId) + " AND " + TTCV_CODE + "=\"" + code + "\" ORDER BY " + TTCV_CODE);
    while (set.next()) {
      result.add(set.getString(1));
    }
    set.close();
    return result;
  }

  public static ArrayList<String> selectCuis(final int mapId) throws SQLException {
    final ArrayList<String> result = new ArrayList<String>();
    final ResultSet set = Database.getInstance().query(Tables.selectWhatFrom(TTCV_CODE, TTCV_TABLE) + Tables.whereIntEquals(TTCV_MAPID, mapId) + " GROUP BY " + TTCV_CODE);
    while (set.next()) {
      result.add(set.getString(1));
    }
    return result;
  }

  public static void drop() {
    Database.getInstance().update(drop(TTCV_TABLE));
  }

  public static void create() {
    Database.getInstance().update(createTermToConceptValuesTable());
  }

}
