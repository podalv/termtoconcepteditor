package com.podalv.ttce.tables;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.podalv.db.Database;
import com.podalv.db.utils.HashUtils;
import com.podalv.ttce.common.Tables;
import com.podalv.ttce.server.Login;

/** Manages the owners table of the ttc mappings
 *
 * @author podalv
 *
 */
public class Owners extends Tables {

  public static final String DEFAULT_PASSWORD = "password";
  public static final String ADMIN_GROUP      = "admin";

  public static boolean checkLogin(final String name, final String password) {
    try {
      return Database.getInstance().stringQueryEquals(selectPasswordFromOwners(name), OWNERS_PASSWORD, HashUtils.calculateHash(password));
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    return false;
  }

  private static final String createOwnersTable() {
    return "CREATE TABLE IF NOT EXISTS " + DATABASE_NAME + "." + OWNERS_TABLE + " (" + //
        "`" + OWNERS_OWNERID + "` int(11) NOT NULL AUTO_INCREMENT," + //
        "`" + OWNERS_NAME + "` varchar(45) DEFAULT NULL," + //
        "`" + OWNERS_PASSWORD + "` TEXT DEFAULT NULL," + //
        "`" + OWNERS_GROUP + "` varchar(255) DEFAULT NULL," + //
        "PRIMARY KEY (`" + OWNERS_OWNERID + "`)," + //
        "UNIQUE KEY `" + OWNERS_OWNERID + "_UNIQUE` (`" + OWNERS_OWNERID + "`));";
  }

  public static boolean isPasswordEqual(final String password1, final String password2) {
    return HashUtils.calculateHash(password1).equals(HashUtils.calculateHash(password2));
  }

  private static final String createOwnerSql(final String owner, final String password) {
    return "INSERT INTO " + DATABASE_NAME + "." + OWNERS_TABLE + " VALUES(DEFAULT, \"" + owner + "\", \"" + password + "\", \"\");";
  }

  public static final void setPassword(final int ownerId, final String newPassword) {
    Database.getInstance().update(
        "UPDATE " + DATABASE_NAME + "." + OWNERS_TABLE + " SET " + OWNERS_PASSWORD + " = \"" + HashUtils.calculateHash(newPassword) + "\" WHERE " + OWNERS_OWNERID + " = "
            + ownerId);
  }

  public static final void setGroup(final int ownerId, final String group) {
    Database.getInstance().update("UPDATE " + DATABASE_NAME + "." + OWNERS_TABLE + " SET " + OWNERS_GROUP + " = \"" + group + "\" WHERE " + OWNERS_OWNERID + " = " + ownerId);
  }

  private static final String selectPasswordFromOwners(final String ownerName) {
    return selectWhatFrom(OWNERS_PASSWORD, OWNERS_TABLE) + whereStringEquals(OWNERS_NAME, ownerName);
  }

  public static final String selectPasswordFromOwners(final int ownerId) throws SQLException {
    final ResultSet set = Database.getInstance().query(selectWhatFrom(OWNERS_PASSWORD, OWNERS_TABLE) + whereIntEquals(OWNERS_OWNERID, ownerId));
    set.next();
    return set.getString(1);
  }

  private static final String selectOwnerIdFromOwners(final String ownerName) {
    return selectWhatFrom(OWNERS_OWNERID, OWNERS_TABLE) + whereStringEquals(OWNERS_NAME, ownerName);
  }

  private static boolean ownerExists(final String name) {
    ResultSet owners = null;
    boolean exists = false;
    try {
      owners = selectAll();
      while (owners.next()) {
        if (owners.getString(OWNERS_NAME).equalsIgnoreCase(name)) {
          exists = true;
          break;
        }
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    finally {
      if (owners != null) {
        Database.closeQuery(owners);
      }
    }
    return exists;
  }

  public static int getOwnerId(final String name) throws SQLException {
    final ResultSet set = Database.getInstance().query(selectOwnerIdFromOwners(name));
    if (set.first()) {
      final int result = set.getInt(1);
      if (set.next()) {
        throw new SQLException("There are more owners with the name '" + name + "'");
      }
      set.close();
      return result;
    }
    throw new SQLException("Owner with the name '" + name + "' not found");
  }

  public static HashMap<Integer, String> getOwners() throws SQLException {
    final HashMap<Integer, String> result = new HashMap<Integer, String>();
    final ResultSet set = Database.getInstance().query(selectWhatFrom("*", OWNERS_TABLE));
    while (set.next()) {
      result.put(set.getInt(OWNERS_OWNERID), set.getString(OWNERS_NAME));
    }
    set.close();
    return result;
  }

  public static String getOwnerName(final int ownerId) throws SQLException {
    final ResultSet set = Database.getInstance().query(selectWhatFrom(OWNERS_NAME, OWNERS_TABLE) + whereIntEquals(OWNERS_OWNERID, ownerId));
    if (set.next()) {
      return set.getString(OWNERS_NAME);
    }
    return "Unknown";
  }

  public static String getOwnerGroup(final int ownerId) throws SQLException {
    final ResultSet set = Database.getInstance().query(selectWhatFrom("*", OWNERS_TABLE) + whereIntEquals(OWNERS_OWNERID, ownerId));
    if (set.first()) {
      final String result = set.getString(OWNERS_GROUP);
      return result == null ? "" : result;
    }
    throw new SQLException("Owner with the id '" + ownerId + "' not found");
  }

  public synchronized static boolean createOwner(final String name, final String password) {
    if (!ownerExists(name)) {
      Database.getInstance().update(createOwnerSql(name, HashUtils.calculateHash(password)));
      return true;
    }
    return false;
  }

  public static ResultSet selectAll() throws SQLException {
    return Database.getInstance().query(Tables.selectWhatFrom("*", OWNERS_TABLE));
  }

  public static void drop() {
    Database.getInstance().update(drop(OWNERS_TABLE));
  }

  public static void create() {
    Database.getInstance().update(createOwnersTable());
  }

  public static boolean isCurrentOwnerAdmin(final HttpServletRequest request) {
    return request.getSession().getAttribute(Login.LOGGED_ADMIN) != null;
  }

}
