package com.podalv.ttce.tables;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.podalv.db.Database;
import com.podalv.maps.string.StringHashSet;
import com.podalv.ttce.common.Tables;

/** Wraps around the terminology tables
 *
 * @author podalv
 *
 */
public class Terminology extends Tables {

  private static String generateCuiOrStatement(final String ... cui) {
    final StringBuilder result = new StringBuilder(" WHERE (");
    if (cui.length == 1) {
      result.append(CUI2STR_CUI).append(" = \"").append(cui[0]).append('"');
    }
    else {
      for (int x = 0; x < cui.length; x++) {
        result.append(CUI2STR_CUI).append(" = \"").append(cui[x]).append('"');
        if (x != cui.length - 1) {
          result.append(" OR ");
        }
      }
    }

    result.append(")");
    return result.toString();
  }

  public static ResultSet selectCuisFromTextLike(final String text) throws SQLException {
    return Database.getInstance().query("SELECT * FROM " + TERMINOLOGY_DATABASE_NAME + "." + STR2CUI_TABLE + Tables.whereStringLike(STR2CUI_STR, text) + " ORDER BY " + STR2CUI_CUI);
  }

  public static ResultSet selectCuisFromTextEqual(final String text) throws SQLException {
    return Database.getInstance().query(
        "SELECT * FROM " + TERMINOLOGY_DATABASE_NAME + "." + STR2CUI_TABLE + Tables.whereStringEquals(STR2CUI_STR, text) + " ORDER BY " + STR2CUI_CUI);
  }

  public static ResultSet selectMultipleCuis(final String ... cui) throws SQLException {
    if (cui.length != 0) {
      return Database.getInstance().query(
          "SELECT * FROM " + TERMINOLOGY_DATABASE_NAME + "." + CUI2STR_TABLE + generateCuiOrStatement(cui) + " ORDER BY " + CUI2STR_CUI + ", " + CUI2STR_STR);
    }
    else {
      return null;
    }
  }

  public static ResultSet selectDominantSyntacticType() throws SQLException {
    return Database.getInstance().query("SELECT * FROM terminology3._nouns WHERE isMax=\"T\"");
  }

  public static ResultSet selectMedlineFrequencies() throws SQLException {
    return Database.getInstance().query("SELECT * FROM " + DATABASE_NAME + ".medline_term_freq");
  }

  public static ResultSet selectAllStrToCui() throws SQLException {
    return Database.getInstance().query("SELECT * FROM terminology4.str2cui INNER JOIN (terminology4._UMLS_MRSTY) ON (terminology4.str2cui.CUI=_UMLS_MRSTY.cui)");
  }

  public static ResultSet selectAllStrToCuiGroupByTermId() throws SQLException {
    return Database.getInstance().query("SELECT * FROM terminology4.str2cui INNER JOIN (terminology4._UMLS_MRSTY) ON (terminology4.str2cui.CUI=_UMLS_MRSTY.cui) GROUP BY tid");
  }

  public static StringHashSet getProcedures() {
    final StringHashSet result = new StringHashSet();
    try {
      final ResultSet set = Database.getInstance().query("SELECT * FROM terminology4.proc");
      while (set.next()) {
        result.add(set.getString(1));
      }
      set.close();
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    return result;
  }

  public static StringHashSet getDrugs() {
    final StringHashSet result = new StringHashSet();
    try {
      final ResultSet set = Database.getInstance().query("SELECT * FROM terminology4.drug");
      while (set.next()) {
        result.add(set.getString(1));
      }
      set.close();
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    return result;
  }

  public static StringHashSet getDevices() {
    final StringHashSet result = new StringHashSet();
    try {
      final ResultSet set = Database.getInstance().query("SELECT * FROM terminology4.device");
      while (set.next()) {
        result.add(set.getString(1));
      }
      set.close();
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    return result;
  }

  public static String getStringFromTidTerm3(final String tid) throws SQLException {
    final ResultSet set = Database.getInstance().query("SELECT str FROM terminology3.str2tid where tid=" + tid);
    if (set.next()) {
      return set.getString(1);
    }
    return null;
  }

  public static String getCuiFromCidTerm3(final String cid) throws SQLException {
    final ResultSet set = Database.getInstance().query("SELECT cui FROM terminology3.str2cid where cid=" + cid);
    if (set.next()) {
      return set.getString(1);
    }
    return null;
  }

  public static String getStringFromTidTerm3(final int tid) throws SQLException {
    final ResultSet set = Database.getInstance().query("SELECT str FROM terminology3.str2tid where tid=" + tid);
    if (set.next()) {
      return set.getString(1);
    }
    return null;
  }

  public static StringHashSet getDiseases() {
    final StringHashSet result = new StringHashSet();
    try {
      final ResultSet set = Database.getInstance().query("SELECT * FROM terminology4.disease");
      while (set.next()) {
        result.add(set.getString(1));
      }
      set.close();
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    return result;
  }

}
