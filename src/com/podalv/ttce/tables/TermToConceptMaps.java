package com.podalv.ttce.tables;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.podalv.db.Database;
import com.podalv.ttce.common.Tables;

/** Manages the term_to_concept_maps table of the ttc mappings
*
* @author podalv
*
*/
public class TermToConceptMaps extends Tables {

  public static final String EXPERIMENTAL_DEFAULT = "Y";
  public static final String VERSION_DEFAULT      = "1";

  private static final String createTermToConceptMapsTable() {
    return "CREATE TABLE IF NOT EXISTS " + DATABASE_NAME + "." + TTCM_TABLE + " (" + //
        "`" + TTCM_MAPID + "` int(10) unsigned NOT NULL AUTO_INCREMENT," + //
        "`" + TTCM_MAPNAME + "` varchar(45) DEFAULT NULL," + //
        "`" + TTCM_OWNER_ID + "` int(11) DEFAULT NULL," + //
        "`" + TTCM_EXPERIMENTAL + "` varchar(45) NOT NULL," + //
        "`" + TTCM_VERSION + "` varchar(45) DEFAULT NULL," + //
        "`" + TTCM_QUERY_TEXT + "` varchar(255) DEFAULT NULL," + //
        "PRIMARY KEY (`" + TTCM_MAPID + "`)," + //
        "UNIQUE KEY `" + TTCM_MAPID + "_UNIQUE` (`" + TTCM_MAPID + "`)" + //
        ") ENGINE=MyISAM DEFAULT CHARSET=utf8;";
  }

  public static synchronized void copy(final int mapId, final int ownerId) throws SQLException {
    final String mapName = TermToConceptMaps.selectSingleMapName(mapId);
    for (int x = 1; x < 1000; x++) {
      final String newMapName = mapName + " copy(" + x + ")";
      if (getMapId(newMapName) == -1) {
        createMapSql(newMapName, ownerId);
        final int newMapId = getMapId(newMapName);
        TermToConceptValues.copyMaps(mapId, newMapId);
        return;
      }
    }
    throw new SQLException("Error creating a new map from an old map");
  }

  public static void deleteMap(final int mapId) {
    Database.getInstance().update("DELETE FROM " + Tables.DATABASE_NAME + "." + TTCV_TABLE + " WHERE " + TTCV_MAPID + " = " + mapId);
    Database.getInstance().update("DELETE FROM " + Tables.DATABASE_NAME + "." + TTCM_TABLE + " WHERE " + TTCM_MAPID + " = " + mapId);
  }

  public static void createMapSql(final String name, final int ownerId) {
    Database.getInstance().update(
        "INSERT INTO " + DATABASE_NAME + "." + TTCM_TABLE + " VALUES(DEFAULT, \"" + name + "\", " + ownerId + ", \"" + EXPERIMENTAL_DEFAULT + "\", \"" + VERSION_DEFAULT
            + "\",\"\");");
  }

  public static void createMapSql(final String name, final String version, final String experimental, final int ownerId, final String queryText) {
    Database.getInstance().update(
        "INSERT INTO " + DATABASE_NAME + "." + TTCM_TABLE + " VALUES(DEFAULT, \"" + name + "\", " + ownerId + ", \"" + experimental + "\", \"" + version + "\", \"" + queryText
            + "\");");
  }

  public static void editMap(final int mapId, final String name, final String version, final String experimental, final String queryText) {
    Database.getInstance().update(
        "UPDATE " + DATABASE_NAME + "." + TTCM_TABLE + " SET " + TTCM_MAPNAME + "= \"" + name + "\", " + TTCM_VERSION + "=\"" + version + "\", " + TTCM_EXPERIMENTAL + "=\""
            + experimental + "\", " + TTCM_QUERY_TEXT + "=\"" + queryText + "\" WHERE " + TTCM_MAPID + "=" + String.valueOf(mapId));
  }

  public static int getMapId(final String mapName) throws SQLException {
    final ResultSet set = Database.getInstance().query(Tables.selectWhatFrom("*", TTCM_TABLE) + Tables.whereStringEquals(TTCM_MAPNAME, mapName));
    int mapId = -1;
    if (set.next()) {
      mapId = set.getInt(TTCM_MAPID);
    }
    set.close();
    return mapId;
  }

  public static void setMapName(final int mapId, final String mapName) {
    Database.getInstance().update("UPDATE " + DATABASE_NAME + "." + TTCM_TABLE + " SET " + TTCM_MAPNAME + "= \"" + mapName + "\" WHERE " + TTCM_MAPID + "=" + mapId);
  }

  public static void setExperimental(final int mapId, final String experimental) {
    Database.getInstance().update("UPDATE " + DATABASE_NAME + "." + TTCM_TABLE + " SET " + TTCM_EXPERIMENTAL + "= \"" + experimental + "\" WHERE " + TTCM_MAPID + "=" + mapId);
  }

  public static void setVersion(final int mapId, final String version) {
    Database.getInstance().update("UPDATE " + DATABASE_NAME + "." + TTCM_TABLE + " SET " + TTCM_VERSION + "= \"" + version + "\" WHERE " + TTCM_MAPID + "=" + mapId);
  }

  public static ResultSet selectAll() throws SQLException {
    return Database.getInstance().query(Tables.selectWhatFrom("*", TTCM_TABLE));
  }

  public static int selectSingleOwnerId(final int mapId) throws SQLException {
    final ResultSet set = Database.getInstance().query(Tables.selectWhatFrom("*", TTCM_TABLE) + Tables.whereIntEquals(TTCM_MAPID, mapId));
    set.next();
    final int result = set.getInt(TTCM_OWNER_ID);
    set.close();
    return result;
  }

  public static String selectSingleMapName(final int mapId) throws SQLException {
    final ResultSet set = Database.getInstance().query(Tables.selectWhatFrom("*", TTCM_TABLE) + Tables.whereIntEquals(TTCM_MAPID, mapId));
    set.next();
    final String result = set.getString(TTCM_MAPNAME);
    set.close();
    return result;
  }

  public static ResultSet selectSingle(final int mapId) throws SQLException {
    return Database.getInstance().query(Tables.selectWhatFrom("*", TTCM_TABLE) + Tables.whereIntEquals(TTCM_MAPID, mapId));
  }

  public static ResultSet selectAllForOwner(final int ownerId) throws SQLException {
    return Database.getInstance().query(Tables.selectWhatFrom("*", TTCM_TABLE) + Tables.whereIntEquals(TTCM_OWNER_ID, ownerId));
  }

  public static void drop() {
    Database.getInstance().update(drop(TTCM_TABLE));
  }

  public static void create() {
    Database.getInstance().update(createTermToConceptMapsTable());
  }

}
