package com.podalv.ttce.tables;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.podalv.db.Database;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.string.IntKeyObjectIterator;
import com.podalv.maps.string.IntKeyObjectMap;
import com.podalv.ttce.common.Tables;

public class Medline extends Tables {

  public static void generateMedlineStats() throws SQLException {
    Database.getInstance().update("DROP TABLE IF EXISTS " + DATABASE_NAME + "." + MEDLINE_TF_TABLE);
    Database.getInstance().update(
        "CREATE TABLE " + DATABASE_NAME + "." + MEDLINE_TF_TABLE + "(\n" + "`" + MEDLINE_TF_TERM + "` text,\n" + "`" + MEDLINE_TF_FREQ + "` double DEFAULT NULL\n" + ")");

    final ResultSet set = Database.getInstance().queryStreaming("SELECT * FROM " + MEDLINE_DATABASE_NAME + "." + MEDLINE_ABSTRACTS_TID_TABLE);
    final IntKeyObjectMap<IntOpenHashSet> tidToArticleId = new IntKeyObjectMap<IntOpenHashSet>();
    final IntOpenHashSet processedArticles = new IntOpenHashSet();
    int rowCnt = 0;
    while (set.next()) {
      final int tid = set.getInt(2);
      IntOpenHashSet pids = tidToArticleId.get(tid);
      if (pids == null) {
        pids = new IntOpenHashSet();
        tidToArticleId.put(tid, pids);
      }
      final int pid = set.getInt(1);
      processedArticles.add(pid);
      pids.add(pid);
      if (rowCnt++ % 10000000 == 0) {
        System.out.println(rowCnt);
      }
    }
    set.close();
    final IntKeyObjectIterator<IntOpenHashSet> i = tidToArticleId.entries();
    final int articleSize = processedArticles.size();
    while (i.hasNext()) {
      i.next();
      Database.getInstance().update(
          "INSERT INTO " + DATABASE_NAME + "." + MEDLINE_TF_TABLE + " VALUES(\"" + Database.escapeSql(Terminology.getStringFromTidTerm3(i.getKey())) + "\" , "
              + (i.getValue().size() / (double) articleSize) + ")");
    }
  }
}
