package com.podalv.ttce.tables;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.podalv.db.Database;
import com.podalv.maps.primitive.map.IntKeyIntOpenHashMap;
import com.podalv.maps.primitive.set.IntOpenHashSet;
import com.podalv.maps.string.IntKeyObjectIterator;
import com.podalv.maps.string.IntKeyObjectMap;
import com.podalv.ttce.common.Tables;

public class Stride5 extends Tables {

  private static IntOpenHashSet uniquePatients = new IntOpenHashSet();

  public static ResultSet selectPatientFrequencies() throws SQLException {
    return Database.getInstance().query("SELECT * from " + DATABASE_NAME + ".patient_freq");
  }

  public static ResultSet selectAll() throws SQLException {
    return Database.getInstance().query("SELECT * FROM stride5.freq LEFT JOIN (terminology3.str2tid) ON (stride5.freq.tid=terminology3.str2tid.tid);");
  }

  private static IntKeyIntOpenHashMap generateVisitToPatientMap() throws SQLException {
    System.out.println("Generating visit to patient map...");
    final IntKeyIntOpenHashMap visitToPatientId = new IntKeyIntOpenHashMap();
    final ResultSet set = Database.getInstance().queryStreaming("SELECT * FROM stride5.note");
    int cnt = 0;
    while (set.next()) {
      visitToPatientId.put(set.getInt(2), set.getInt(1));
      uniquePatients.add(set.getInt(1));
      if (cnt++ % 100000 == 0) {
        System.out.println(cnt);
      }
    }
    set.close();
    return visitToPatientId;
  }

  private static void createResultTable() {
    Database.getInstance().update("DROP TABLE IF EXISTS " + DATABASE_NAME + ".patient_freq");
    Database.getInstance().update("CREATE TABLE " + DATABASE_NAME + ".patient_freq (\n" + "`str` text,\n" + "`freq` double DEFAULT NULL)\n");
  }

  public static void generatePatientFrequencies() throws SQLException {
    createResultTable();
    System.out.println("Generating patient frequencies...");
    final IntKeyIntOpenHashMap visitToPatientId = generateVisitToPatientMap();
    final IntKeyObjectMap<IntOpenHashSet> termToPatientId = new IntKeyObjectMap<IntOpenHashSet>();
    final ResultSet set = Database.getInstance().queryStreaming("SELECT * FROM stride5.mgrep WHERE negated = 0 AND familyHistory = 0");
    long cnt = 0;
    while (set.next()) {
      final int termId = set.getInt(2);
      IntOpenHashSet patients = termToPatientId.get(termId);
      if (patients == null) {
        patients = new IntOpenHashSet();
        termToPatientId.put(termId, patients);
      }
      patients.add(visitToPatientId.get(set.getInt(1)));
      if (cnt++ % 10000000 == 0) {
        System.out.println(cnt);
      }
    }
    set.close();
    System.out.println("Writing results...");
    final IntKeyObjectIterator<IntOpenHashSet> iterator = termToPatientId.entries();
    while (iterator.hasNext()) {
      iterator.next();
      Database.getInstance().update(
          "INSERT INTO " + DATABASE_NAME + ".patient_freq VALUES(\"" + Database.escapeSql(Terminology.getStringFromTidTerm3(iterator.getKey())) + "\", "
              + (iterator.getValue().size() / (double) uniquePatients.size()) + ")");
    }
  }
}
