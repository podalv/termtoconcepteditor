package com.podalv.ttce.common;

/** Allows constructing reusable SQL queries
 *
 * @author podalv
 *
 */
public class Tables {

  protected static String       DATABASE_NAME               = "term_concept_map";
  protected static final String TERMINOLOGY_DATABASE_NAME   = "terminology4";
  protected static final String MEDLINE_DATABASE_NAME       = "medline";
  public static final String    CUI2STR_TABLE               = "cui2str";
  public static final String    CUI2STR_CUI                 = "cui";
  public static final String    CUI2STR_STR                 = "str";
  public static final String    STR2CUI_TABLE               = "str2cui";
  public static final String    STR2CUI_CUI                 = "CUI";
  public static final String    STR2CUI_STR                 = "str";
  public static final String    STR2CUI_ONTOLOGY            = "ontology";
  public static final String    STR2CUI_SUPPRESS            = "suppress_umls";
  public static final String    ADMINS_TABLE                = "admins";
  public static final String    ADMINS_OWNERID              = "ownerid";
  public static final String    OWNERS_TABLE                = "owners";
  public static final String    OWNERS_OWNERID              = "ownerid";
  public static final String    OWNERS_NAME                 = "name";
  public static final String    OWNERS_PASSWORD             = "password";
  public static final String    OWNERS_GROUP                = "ownergroup";
  public static final String    TTCM_TABLE                  = "term_to_concept_maps";
  public static final String    TTCM_MAPID                  = "mapid";
  public static final String    TTCM_MAPNAME                = "mapname";
  public static final String    TTCM_OWNER_ID               = "ownerid";
  public static final String    TTCM_EXPERIMENTAL           = "experimental";
  public static final String    TTCM_VERSION                = "version";
  public static final String    TTCM_QUERY_TEXT             = "querytext";
  public static final String    TTCV_TABLE                  = "term_to_concept_values";
  public static final String    TTCV_MAPID                  = "mapid";
  public static final String    TTCV_CODE                   = "code";
  public static final String    TTCV_TEXT                   = "text";
  public static final String    MEDLINE_TF_TABLE            = "medline_term_freq";
  public static final String    MEDLINE_TF_TERM             = "term";
  public static final String    MEDLINE_TF_FREQ             = "term_freq";
  public static final String    MEDLINE_ABSTRACTS_TID_TABLE = "abstracts_tid";

  public static void setDatabaseName(final String databaseName) {
    DATABASE_NAME = databaseName;
  }

  protected static final String selectWhatFrom(final String what, final String table) {
    if (table.indexOf('.') == -1) {
      return "SELECT " + what + " FROM " + DATABASE_NAME + "." + table;
    }
    else {
      return "SELECT " + what + " FROM " + table;
    }
  }

  protected static final String deleteFromWhere(final String table) {
    return "DELETE FROM " + DATABASE_NAME + "." + table;
  }

  protected static final String whereStringEquals(final String parameter, final String value) {
    return " WHERE " + parameter + " = \"" + value + "\"";
  }

  protected static final String whereStringLike(final String parameter, final String value) {
    return " WHERE " + parameter + " like \"%" + value + "%\"";
  }

  protected static final String whereIntEquals(final String parameter, final int value) {
    return " WHERE " + parameter + " = " + value;
  }

  protected static final String drop(final String table) {
    return "DROP TABLE IF EXISTS " + DATABASE_NAME + "." + table;
  }

}
