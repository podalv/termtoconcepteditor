package com.podalv.ttce.common;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.LinkedList;

import com.podalv.maps.string.StringHashSet;
import com.podalv.maps.string.StringKeyIntMap;
import com.podalv.maps.string.StringKeyObjectIterator;
import com.podalv.maps.string.StringKeyObjectMap;
import com.podalv.ttce.tables.Stride5;
import com.podalv.ttce.tables.Terminology;

/** Contains statistics about words taken from
 *
 * @author podalv
 *
 */
public class StringStatsDictionary {

  public final static int                           STRIDE_TERM_POS           = 0;
  public final static int                           STRIDE_PATIENT_POS        = 1;
  public final static int                           SYNTACTIC_TYPE_POS        = 2;
  public final static int                           MEDLINE_POS               = 3;

  public final static int                           AMBIGUOUS_CUI_CNT         = 0;
  public final static int                           AMBIGUOUS_STY_CNT         = 1;
  public final static int                           AMBIGUOUS_SG_CNT          = 2;

  private static int                                maxStride5SingleFrequency = 0;

  private final static StringKeyObjectMap<double[]> textToStats               = new StringKeyObjectMap<double[]>();
  private final static StringKeyObjectMap<byte[]>   textToAmbiguityStats      = new StringKeyObjectMap<byte[]>();
  private final static StringHashSet                diseases                  = Terminology.getDiseases();
  private final static StringHashSet                procedures                = Terminology.getProcedures();
  private final static StringHashSet                drugs                     = Terminology.getDrugs();
  private final static StringHashSet                devices                   = Terminology.getDevices();
  private final static StringKeyIntMap              cuiToTermCount            = new StringKeyIntMap();

  public static StringStats getStats(final String text) {
    return new StringStats(textToStats.get(text), textToAmbiguityStats.get(text));
  }

  public static int getEligibleTermCnt(final String cui) {
    final Integer result = cuiToTermCount.get(cui);
    return result == null ? 0 : result.intValue();
  }

  private static void recordAmbiguity(String text, final LinkedList<String> cuis, final StringHashSet semanticTypes) {
    text = text.toLowerCase();
    final int uniqueCuiCnt = cuis.size();
    final int uniqueSemanticTypes = semanticTypes.size();
    textToAmbiguityStats.put(text, new byte[] {(byte) uniqueCuiCnt, (byte) uniqueSemanticTypes, (byte) calculateSemanticGroupCnt(cuis)});
  }

  private static int calculateSemanticGroupCnt(final LinkedList<String> cuis) {
    final boolean[] groups = new boolean[4];
    for (final String cui : cuis) {
      if (procedures.contains(cui)) {
        groups[0] = true;
      }
      if (devices.contains(cui)) {
        groups[1] = true;
      }
      if (drugs.contains(cui)) {
        groups[2] = true;
      }
      if (diseases.contains(cui)) {
        groups[3] = true;
      }
    }
    return summarizeCounts(groups);
  }

  private static int summarizeCounts(final boolean[] groups) {
    int semanticGroupCnt = 0;
    for (int x = 0; x < groups.length; x++) {
      if (groups[x]) {
        semanticGroupCnt++;
      }
    }
    return semanticGroupCnt;
  }

  private static void addTermToStats(String term, final double value, final int pos) {
    term = term.toLowerCase();
    double[] array = textToStats.get(term);
    if (array == null) {
      array = new double[4];
      Arrays.fill(array, -1);
      textToStats.put(term, array);
    }
    array[pos] = value;
  }

  private static void normalizeCounts() {
    System.out.println("Normalizing values...");
    final StringKeyObjectIterator<double[]> iterator = textToStats.entries();
    while (iterator.hasNext()) {
      iterator.next();
      final double[] values = iterator.getValue();
      values[STRIDE_TERM_POS] = values[STRIDE_TERM_POS] / maxStride5SingleFrequency;
    }
    System.out.println("Done");
  }

  private static void readStride5PatientFrequencies() throws SQLException {
    final ResultSet set = Stride5.selectPatientFrequencies();
    while (set.next()) {
      addTermToStats(set.getString(1), set.getDouble(2), STRIDE_PATIENT_POS);
    }
    set.close();
  }

  public static void readData() throws SQLException {
    readStride5();
    readStride5PatientFrequencies();
    readDominantSyntacticType();
    readMedlineFrequencies();
    readAmbiguities();
    freeMemory();
    normalizeCounts();
    calculateTermToCui();
  }

  private static void calculateTermToCui() throws SQLException {
    System.out.println("Calculating cui to term counts...");
    final ResultSet set = Terminology.selectAllStrToCuiGroupByTermId();
    while (set.next()) {
      final String cui = set.getString(3);
      final String term = set.getString(2).toLowerCase();
      if (textToStats.get(term) != null) {
        cuiToTermCount.increment(cui, 1);
      }
    }
    set.close();
    System.out.println("Done");
  }

  private static void readStride5() throws SQLException {
    System.out.println("Reading 'stride5' database...");
    final ResultSet set = Stride5.selectAll();
    while (set.next()) {
      maxStride5SingleFrequency = Math.max(maxStride5SingleFrequency, set.getInt(2));
      addTermToStats(set.getString(10), set.getInt(2), STRIDE_TERM_POS);
    }
    set.close();
  }

  private static void freeMemory() {
    procedures.clear();
    devices.clear();
    drugs.clear();
    diseases.clear();
  }

  private static void readAmbiguities() throws SQLException {
    ResultSet set;
    System.out.println("Reading 'terminology4.str2cui' table...");
    set = Terminology.selectAllStrToCui();
    String text = null;
    final LinkedList<String> assignedCuis = new LinkedList<String>();
    final StringHashSet semanticTypes = new StringHashSet();
    while (set.next()) {
      if (text == null) {
        text = set.getString(2);
      }
      if (!text.equals(set.getString(2))) {
        if (assignedCuis.size() > 1) {
          recordAmbiguity(text, assignedCuis, semanticTypes);
        }
        assignedCuis.clear();
        semanticTypes.clear();
      }
      assignedCuis.add(set.getString(3));
      semanticTypes.add(set.getString(7));
      text = set.getString(2);
    }
    if (assignedCuis.size() > 1) {
      recordAmbiguity(text, assignedCuis, semanticTypes);
    }
    set.close();
  }

  private static void readMedlineFrequencies() throws SQLException {
    ResultSet set;
    System.out.println("Reading 'term_to_concept_map.medline_term_freq' table...");
    set = Terminology.selectMedlineFrequencies();
    while (set.next()) {
      addTermToStats(set.getString(1), set.getDouble(2), MEDLINE_POS);
    }
    set.close();
  }

  private static void readDominantSyntacticType() throws SQLException {
    ResultSet set;
    System.out.println("Reading 'terminology3._nouns' table...");
    set = Terminology.selectDominantSyntacticType();
    while (set.next()) {
      addTermToStats(set.getString(1), (set.getDouble(4) / 100), SYNTACTIC_TYPE_POS);
    }
    set.close();
  }
}
