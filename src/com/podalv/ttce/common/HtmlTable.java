package com.podalv.ttce.common;

import java.util.ArrayList;

/** A table object that can be passed as a parameter to the rendering page method
 *
 * @author podalv
 *
 */
public class HtmlTable {

  private final ArrayList<ArrayList<String>> rows      = new ArrayList<ArrayList<String>>();
  private int                                maxColCnt = 0;

  public HtmlTable(final String ... headers) {
    addRow(headers);
  }

  public void addRow(final String ... cells) {
    final ArrayList<String> values = new ArrayList<String>();
    for (final String c : cells) {
      values.add(c);
    }
    rows.add(values);
    maxColCnt = Math.max(maxColCnt, values.size());
  }

  public int rowCnt() {
    return rows.size();
  }

  public int getHeaderColCnt() {
    return rows.get(0).size();
  }

  public int getColCnt() {
    return maxColCnt;
  }

  public String getCell(final int col, final int row) {
    if (col > rows.get(row).size() - 1) {
      return "";
    }
    return rows.get(row).get(col);
  }
}
