package com.podalv.ttce.common;

import java.text.DecimalFormat;

/** Encapsulates statistics about the string (frequencies, ambiguity information, etc.)
 *
 * @author podalv
 *
 */
public class StringStats {

  private double stride5TermFrequencies    = -1;
  private double stride5PatientFrequencies = -1;
  private double syntacticType             = -1;
  private double medline                   = -1;
  private byte   uniqueCuiCnt              = -1;
  private byte   semanticTypeCnt           = -1;
  private byte   semanticGroupCnt          = -1;

  public StringStats(final double[] frequencyStats, final byte[] ambiguityStats) {
    if (frequencyStats != null) {
      stride5TermFrequencies = frequencyStats[StringStatsDictionary.STRIDE_TERM_POS];
      stride5PatientFrequencies = frequencyStats[StringStatsDictionary.STRIDE_PATIENT_POS];
      medline = frequencyStats[StringStatsDictionary.MEDLINE_POS];
      syntacticType = frequencyStats[StringStatsDictionary.SYNTACTIC_TYPE_POS];
    }
    if (ambiguityStats != null) {
      uniqueCuiCnt = ambiguityStats[StringStatsDictionary.AMBIGUOUS_CUI_CNT];
      semanticTypeCnt = ambiguityStats[StringStatsDictionary.AMBIGUOUS_STY_CNT];
      semanticGroupCnt = ambiguityStats[StringStatsDictionary.AMBIGUOUS_SG_CNT];
    }
  }

  public boolean isEmpty() {
    return medline == -1 && stride5TermFrequencies == -1 && syntacticType == -1;
  }

  private String doubleToStr(final double value) {
    return value < 0 ? "" : String.valueOf(new DecimalFormat("###.##").format(value * 100));
  }

  private String byteToStr(final byte value) {
    return value < 0 ? "" : String.valueOf(value);
  }

  public String getMedline() {
    return doubleToStr(medline);
  }

  public String getSemanticGroupCnt() {
    return byteToStr(semanticGroupCnt);
  }

  public String getSemanticTypeCnt() {
    return byteToStr(semanticTypeCnt);
  }

  public String getStride5All() {
    return doubleToStr(stride5PatientFrequencies);
  }

  public String getStride5Single() {
    return doubleToStr(stride5TermFrequencies);
  }

  public String getSyntacticType() {
    return doubleToStr(syntacticType);
  }

  public String getUniqueCuiCnt() {
    return byteToStr(uniqueCuiCnt);
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }

  @Override
  public boolean equals(final Object obj) {
    return super.equals(obj);
  }
}
