package com.podalv.ttce.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

/** Templates of Html pages
 *  Language specifics:
 *
 *  <parameter>name</parameter> is replaced with the request.getParameter(name)
 *  <session>name</session> is replaced with request.getSession.getAttribute(name)
 *  <file>name</file> content of the file located in templates is put here
 *  <ifNotNull session="name">TEXT</ifNotNull> if the request.getSession.getAttribute(name) is not null, TEXT is displayed, otherwise all is skipped
 *
 *  <trEach table="tableName">
 *    <td><data>columnName</data></td>
 *    <td><data>columnName</data></td>
 *  </trEach>
 *  For each row in the table specified by the tableName, columnNames are filled in the specified positions
 *
 *
 * @author podalv
 *
 */
public class HtmlCommon {

  public static enum TEMPLATE {
    LOGIN, CHANGE_PASSWORD, MAPS, DELETE_MAP, EDIT_MAP, DISPLAY_MAPPINGS, EDIT_MAPPING, SEARCH_MAPPING, ADMIN, COPY_MAP_AS
  };
  private static HashMap<File, String>     cache                   = new HashMap<File, String>();
  private static HashMap<TEMPLATE, String> templates               = new HashMap<TEMPLATE, String>();
  public static final String               DATA_TAG                = "data";
  public static final String               FILE_TAG                = "file";
  public static final String               TABLE_TAG               = "tableContent";
  public static final String               TABLE_HEADER_TAG        = "tableHeader";
  public static final String               TABLE_ROW_TAG           = "tableRow";
  public static final String               TABLE_COL_TAG           = "tableCol";
  public static final String               CONTENT_TAG             = "content";
  /** Not null and not empty **/
  public static final String               IFNOTNULL_TAG           = "ifNotNull";
  /** Null or empty **/
  public static final String               IFNULL_TAG              = "ifNull";
  public static final String               PARAMETER_TAG           = "parameter";
  public static final String               SESSION_TAG             = "session";
  public static final String               TREACH_TAG              = "trEach";
  public static final String               TTCM_VALUES_TABLE_CLASS = "termToConceptMapValuesTable";
  public static final String               TTCM_VALUES_SELECTED    = "selectedString";
  public static final String               TTCM_VALUES_NORMAL      = "normalString";

  static {
    templates.put(TEMPLATE.LOGIN, "login");
    templates.put(TEMPLATE.CHANGE_PASSWORD, "change_password");
    templates.put(TEMPLATE.MAPS, "maps");
    templates.put(TEMPLATE.DELETE_MAP, "delete_map");
    templates.put(TEMPLATE.EDIT_MAP, "edit_map");
    templates.put(TEMPLATE.DISPLAY_MAPPINGS, "display_mappings");
    templates.put(TEMPLATE.EDIT_MAPPING, "edit_mapping");
    templates.put(TEMPLATE.SEARCH_MAPPING, "search_mapping");
    templates.put(TEMPLATE.ADMIN, "admin");
    templates.put(TEMPLATE.COPY_MAP_AS, "copy_map_as");
  }

  public static HashMap<String, Object> createSingleValueMap(final String key, final Object value) {
    final HashMap<String, Object> result = new HashMap<String, Object>();
    result.put(key, value);
    return result;
  }

  public static String getOpenTag(final String tag) {
    return "<" + tag + ">";
  }

  public static String getCloseTag(final String tag) {
    return "</" + tag + ">";
  }

  private static String readTemplate(final File file) throws IOException {
    final StringBuilder result = new StringBuilder();
    final BufferedReader reader = new BufferedReader(new FileReader(file));
    String line;
    while ((line = reader.readLine()) != null) {
      int pos = 0;
      boolean modifiedLine = false;
      while (true) {
        pos = line.indexOf(getOpenTag(FILE_TAG), pos);
        if (pos != -1) {
          final int pos1 = line.indexOf(getCloseTag(FILE_TAG));
          if (pos1 != -1) {
            result.append(line.substring(0, pos) + readTemplate(new File(new File("templates"), line.substring(pos + getOpenTag(FILE_TAG).length(), pos1)))
                + line.substring(pos1 + getCloseTag(FILE_TAG).length()));
            modifiedLine = true;
          }
          else {
            break;
          }
        }
        if (pos == -1) {
          break;
        }
        pos++;
      }
      if (!modifiedLine) {
        result.append(line + "\n");
      }
    }
    reader.close();
    return result.toString();
  }

  private static String getTemplate(final File file) throws IOException {
    String value = cache.get(file);
    if (value == null) {
      cache.put(file, readTemplate(file));
      value = cache.get(file);
    }
    return value;
  }

  private static File getFile(final String fileName) {
    return new File(new File("templates"), fileName);
  }

  private static String getNearerString(final String findInString, final String string1, final String string2, final int pos) {
    final int pos1 = findInString.indexOf(string1, pos);
    final int pos2 = findInString.indexOf(string2, pos);
    if (pos1 > 0 && pos2 > 0) {
      return pos1 < pos2 ? string1 : string2;
    }
    else if (pos1 < 0 && pos2 < 0) {
      return null;
    }
    else if (pos1 >= 0) {
      return string1;
    }
    return string2;
  }

  private static String renderTemplate(final String templateText, final HttpServletRequest request) {
    String result = templateText;
    int pos = 0;
    while (true) {
      pos = result.indexOf(getOpenTag(PARAMETER_TAG), pos);
      boolean fnd = false;
      if (pos != -1) {
        final int pos1 = result.indexOf(getCloseTag(PARAMETER_TAG), pos);
        if (pos1 != -1) {
          final String parameter = request.getParameter(result.substring(pos + getOpenTag(PARAMETER_TAG).length(), pos1));
          result = result.substring(0, pos) + (parameter != null ? parameter : "") + result.substring(pos1 + getCloseTag(PARAMETER_TAG).length());
          fnd = true;
        }
      }
      pos = result.indexOf(getOpenTag(SESSION_TAG), pos);
      if (pos != -1) {
        final int pos1 = result.indexOf(getCloseTag(SESSION_TAG), pos);
        if (pos1 != -1) {
          final String parameter = (String) request.getSession().getAttribute(result.substring(pos + getOpenTag(SESSION_TAG).length(), pos1));
          result = result.substring(0, pos) + (parameter != null ? parameter : "") + result.substring(pos1 + getCloseTag(SESSION_TAG).length());
          fnd = true;
        }
      }
      pos = result.indexOf("<" + IFNOTNULL_TAG, pos);
      if (pos != -1) {
        final String closerString = getNearerString(result, "session=\"", "parameter=\"", pos);
        if (closerString.equals("session=\"")) {
          final int sessionPos = result.indexOf("session=\"", pos);
          final int sessionEndPos = result.indexOf('"', sessionPos + "session=\"".length() + 1);
          final String sessionValue = result.substring(sessionPos + "session=\"".length(), result.indexOf('"', sessionPos + "session=\"".length() + 1));
          final int pos1 = result.indexOf("</" + IFNOTNULL_TAG + ">", pos);
          if (pos1 != -1) {
            if (request.getSession().getAttribute(sessionValue) == null || request.getSession().getAttribute(sessionValue).toString().isEmpty()) {
              result = result.substring(0, pos) + result.substring(pos1 + 3 + IFNOTNULL_TAG.length());
            }
            else {
              result = result.substring(0, pos) + renderTemplate(result.substring(sessionEndPos + 2, pos1), request) + result.substring(pos1 + 3 + IFNOTNULL_TAG.length());
            }
            fnd = true;
          }
        }
        else if (closerString.equals("parameter=\"")) {
          final int sessionPos = result.indexOf("parameter=\"", pos);
          final int sessionEndPos = result.indexOf('"', sessionPos + "parameter=\"".length() + 1);
          final String sessionValue = result.substring(sessionPos + "parameter=\"".length(), result.indexOf('"', sessionPos + "parameter=\"".length() + 1));
          final int pos1 = result.indexOf("</" + IFNOTNULL_TAG + ">", pos);
          if (pos1 != -1) {
            if (request.getParameter(sessionValue) == null || request.getParameter(sessionValue).isEmpty()) {
              result = result.substring(0, pos) + result.substring(pos1 + 3 + IFNOTNULL_TAG.length());
            }
            else {
              result = result.substring(0, pos) + renderTemplate(result.substring(sessionEndPos + 2, pos1), request) + result.substring(pos1 + 3 + IFNOTNULL_TAG.length());
            }
            fnd = true;
          }
        }
      }
      pos = result.indexOf("<" + IFNULL_TAG, pos);
      if (pos != -1) {
        final String closerString = getNearerString(result, "session=\"", "parameter=\"", pos);
        if (closerString.equals("session=\"")) {
          final int sessionPos = result.indexOf("session=\"", pos);
          final int sessionEndPos = result.indexOf('"', sessionPos + "session=\"".length() + 1);
          final String sessionValue = result.substring(sessionPos + "session=\"".length(), result.indexOf('"', sessionPos + "session=\"".length() + 1));
          final int pos1 = result.indexOf("</" + IFNULL_TAG + ">", pos);
          if (pos1 != -1) {
            if (request.getParameter(sessionValue) == null || request.getParameter(sessionValue).isEmpty()) {
              result = result.substring(0, pos) + renderTemplate(result.substring(sessionEndPos + 2, pos1), request) + result.substring(pos1 + 3 + IFNULL_TAG.length());
            }
            else {
              result = result.substring(0, pos) + result.substring(pos1 + 3 + IFNULL_TAG.length());
            }
            fnd = true;
          }
        }
        else if (closerString.equals("parameter=\"")) {
          final int sessionPos = result.indexOf("parameter=\"", pos);
          final int sessionEndPos = result.indexOf('"', sessionPos + "parameter=\"".length() + 1);
          final String sessionValue = result.substring(sessionPos + "parameter=\"".length(), result.indexOf('"', sessionPos + "parameter=\"".length() + 1));
          final int pos1 = result.indexOf("</" + IFNULL_TAG + ">", pos);
          if (pos1 != -1) {
            if (request.getParameter(sessionValue) == null || request.getParameter(sessionValue).isEmpty()) {
              result = result.substring(0, pos) + renderTemplate(result.substring(sessionEndPos + 2, pos1), request) + result.substring(pos1 + 3 + IFNULL_TAG.length());
            }
            else {
              result = result.substring(0, pos) + result.substring(pos1 + 3 + IFNULL_TAG.length());
            }
            fnd = true;
          }
        }
      }
      if (!fnd) {
        break;
      }
      pos++;
    }
    return result;
  }

  private static String renderTable(final String tableTemplate, final HtmlTable table) {
    String result = tableTemplate;
    int pos = 0;
    while (true) {
      pos = result.indexOf(getOpenTag(TABLE_HEADER_TAG), pos);
      if (pos != -1) {
        final int pos1 = result.indexOf(getCloseTag(TABLE_HEADER_TAG), pos);
        if (pos1 != -1) {
          final String parameter = table.getCell(Integer.parseInt(result.substring(pos + getOpenTag(TABLE_HEADER_TAG).length(), pos1)), 0);
          result = result.substring(0, pos) + (parameter != null ? parameter : "") + result.substring(pos1 + getCloseTag(TABLE_HEADER_TAG).length());
        }
        else {
          break;
        }
      }
      else {
        break;
      }
    }
    final int rowStart = result.indexOf(getOpenTag(TABLE_ROW_TAG));
    final int rowEnd = result.indexOf(getCloseTag(TABLE_ROW_TAG)) + getCloseTag(TABLE_ROW_TAG).length();
    final String rowContents = result.substring(rowStart + getOpenTag(TABLE_ROW_TAG).length(), result.indexOf(getCloseTag(TABLE_ROW_TAG)));
    final StringBuilder generatedRows = new StringBuilder();
    for (int x = 1; x < table.rowCnt(); x++) {
      String generatedRow = rowContents;
      pos = 0;
      while (true) {
        pos = generatedRow.indexOf(getOpenTag(TABLE_COL_TAG), pos);
        if (pos != -1) {
          final int pos1 = generatedRow.indexOf(getCloseTag(TABLE_COL_TAG), pos);
          if (pos1 != -1) {
            final String parameter = table.getCell(Integer.parseInt(generatedRow.substring(pos + getOpenTag(TABLE_COL_TAG).length(), pos1)), x);
            generatedRow = generatedRow.substring(0, pos) + (parameter != null ? parameter : "") + generatedRow.substring(pos1 + getCloseTag(TABLE_COL_TAG).length());
          }
          else {
            break;
          }
        }
        else {
          break;
        }
      }
      generatedRows.append("<tr>" + generatedRow + "</tr>");
    }
    result = result.substring(0, rowStart) + generatedRows + result.substring(rowEnd);
    return result;
  }

  private static String injectValues(final String template, final HashMap<String, Object> values) {
    String result = template;
    int pos = 0;
    while (true) {
      pos = result.indexOf(getOpenTag(CONTENT_TAG), pos);
      if (pos != -1) {
        final int pos1 = result.indexOf(getCloseTag(CONTENT_TAG), pos);
        if (pos1 != -1) {
          final String parameter = (String) values.get(result.substring(pos + getOpenTag(CONTENT_TAG).length(), pos1));
          result = result.substring(0, pos) + (parameter != null ? parameter : "") + result.substring(pos1 + getCloseTag(CONTENT_TAG).length());
        }
        else {
          break;
        }
      }
      else {
        break;
      }
    }

    pos = 0;
    while (true) {
      pos = result.indexOf("<" + TABLE_TAG, pos);
      if (pos != -1) {
        final int nameStart = result.indexOf("name=\"", pos);
        final String name = result.substring(nameStart + "name=\"".length(), result.indexOf("\">", nameStart));
        final String tableContent = result.substring(result.indexOf("\">", nameStart) + 2, result.indexOf(getCloseTag(TABLE_TAG)));
        String renderedTable = "";
        if (values.get(name) instanceof List) {
          @SuppressWarnings("unchecked")
          final List<HtmlTable> tables = (List<HtmlTable>) values.get(name);
          final StringBuilder tablesText = new StringBuilder();
          for (final HtmlTable table : tables) {
            tablesText.append(renderTable(tableContent, table));
          }
          renderedTable = tablesText.toString();
        }
        else if (values.get(name) instanceof HtmlTable) {
          renderedTable = renderTable(tableContent, (HtmlTable) values.get(name));
        }
        result = result.substring(0, pos) + renderedTable + result.substring(result.indexOf(getCloseTag(TABLE_TAG)) + getCloseTag(TABLE_TAG).length(), result.length());
      }
      else {
        break;
      }
    }

    return result;
  }

  public static String generatePage(final TEMPLATE template, final HttpServletRequest request) throws IOException {
    return generatePage(template, request, new HashMap<String, Object>());
  }

  public static String generatePage(final TEMPLATE template, final HttpServletRequest request, final HashMap<String, Object> values) throws IOException {
    return injectValues(renderTemplate(getTemplate(getFile(templates.get(template))), request), values);
  }

}