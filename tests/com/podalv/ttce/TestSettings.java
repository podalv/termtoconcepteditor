package com.podalv.ttce;

import java.sql.SQLException;

import com.podalv.db.Database;
import com.podalv.db.datastructures.ConnectionSettings;
import com.podalv.db.datastructures.LoginCredentials;
import com.podalv.ttce.common.Tables;

public class TestSettings {

  public static final String CONNECTION    = "jdbc:mysql://localhost:3306";
  public static final String USER          = "root";
  public static final String PASSWORD      = "root";
  public static final String TEST_DATABASE = "podalv_test";

  static {
    Tables.setDatabaseName(TEST_DATABASE);
  }

  public static ConnectionSettings getTestSettings() {
    return new ConnectionSettings(CONNECTION, new LoginCredentials(USER, PASSWORD));
  }

  public static void init() throws SQLException {
    if (Database.getInstance() == null) {
      Database.create(getTestSettings());
    }
  }
}
